﻿using System;
using GazePswd;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class PointTest
    {
        [TestMethod]
        public void Serialized_equal()
        {
            Point p = new Point();
            p.Coords = new Coords(1, 2);
            p.Time = DateTime.Now;
            p.PixelCoords = new Coords(3, 4);
            p.CoordAmount = new Coords(5, 6);
            p.PixelBound =  new Coords(7, 8);
            //Screencoords deliberately left out, as they are not serialized

            String p_string = p.ToString();
            Point p_deserialized = Point.FromString(p_string);

            Assert.AreEqual(p.Coords, p_deserialized.Coords);
            Assert.AreEqual(p.Time, p_deserialized.Time);
            Assert.AreEqual(p.PixelCoords, p_deserialized.PixelCoords);
            Assert.AreEqual(p.CoordAmount, p_deserialized.CoordAmount);
            Assert.AreEqual(p.PixelBound, p_deserialized.PixelBound);
        }
    }
}
