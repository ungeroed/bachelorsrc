﻿using System;
using GazePswd;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class DiscreteFrechetDistanceTest
    {
        IPassword regularPwd_1;
        IPassword regularPwd_2;
        IPassword regularPwd_3;

        IPassword regularPwd_1_reversed;

        [TestMethod]
        public void NameTest()
        {
            DiscreteFrechetAnalyser da = new DiscreteFrechetAnalyser();
            String expected_name = "Discrete Fretchet Distance";
            Assert.AreEqual(expected_name, da.Name);
        }

        [TestMethod]
        public void Match_is_reflexive()
        {
            DiscreteFrechetAnalyser da = new DiscreteFrechetAnalyser();         
            Assert.AreEqual(0d, da.Match((OriginalPswd) regularPwd_1, (AttemptPswd) regularPwd_1));
        }

        [TestMethod]
        public void Match_distance_horiz()
        {
            double expected_distance = 1d;
            DiscreteFrechetAnalyser da = new DiscreteFrechetAnalyser();

            Assert.AreEqual(expected_distance, da.Match((OriginalPswd)regularPwd_1, (AttemptPswd)regularPwd_2));
        }

        [TestMethod]
        public void Match_distance_diag()
        {
            double expected_distance = Math.Sqrt(2);
            DiscreteFrechetAnalyser da = new DiscreteFrechetAnalyser();

            Assert.AreEqual(expected_distance, da.Match((OriginalPswd)regularPwd_1, (AttemptPswd)regularPwd_3));
        }

        [TestMethod]
        public void Match_is_symmetric()
        {
            DiscreteFrechetAnalyser da = new DiscreteFrechetAnalyser();

            double pw1_pw2 = da.Match((OriginalPswd)regularPwd_1, (AttemptPswd)regularPwd_2);
            double pw2_pw1 = da.Match((OriginalPswd)regularPwd_2, (AttemptPswd)regularPwd_1);

            Assert.AreEqual(pw1_pw2, pw2_pw1);
        }

        [TestMethod]
        public void Match_reverse_not_equal()
        {
            DiscreteFrechetAnalyser da = new DiscreteFrechetAnalyser();

            double diff = da.Match((OriginalPswd)regularPwd_1, (AttemptPswd)regularPwd_1_reversed);

            Assert.AreNotEqual(0d, diff);
        }

        [TestInitialize]
        public void setupPasswords()
        {
            //Copied from a real-life recorded password on an image
            regularPwd_1 = new SimplePassword();
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 7) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_1.Add(new Point() { Coords = new Coords(6, 7) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 7) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 7) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 7) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 5) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 4) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 3) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(11, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(7, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(6, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(5, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(4, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(3, 0) });
            regularPwd_1.Add(new Point() { Coords = new Coords(3, 1) });
            regularPwd_1.Add(new Point() { Coords = new Coords(3, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(3, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(4, 2) });
            regularPwd_1.Add(new Point() { Coords = new Coords(4, 3) });

            //Hand written based on previous password
            regularPwd_2 = new SimplePassword();
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 7) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_2.Add(new Point() { Coords = new Coords(6, 7) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 7) });
            
            //change here! - from 11, 7 to 12, 7
            regularPwd_2.Add(new Point() { Coords = new Coords(12, 7) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 7) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 5) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 4) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 3) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(11, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(7, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(6, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(5, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(4, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(3, 0) });
            regularPwd_2.Add(new Point() { Coords = new Coords(3, 1) });
            regularPwd_2.Add(new Point() { Coords = new Coords(3, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(3, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(4, 2) });
            regularPwd_2.Add(new Point() { Coords = new Coords(4, 3) });

            //Hand written based on previous recorded password
            regularPwd_3 = new SimplePassword();
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 7) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 6) });

            //change here! - 6,7 to 7,8
            regularPwd_3.Add(new Point() { Coords = new Coords(7, 8) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 7) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 7) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 7) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 6) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 5) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 4) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 3) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(11, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(10, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(7, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(6, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(5, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(4, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(3, 0) });
            regularPwd_3.Add(new Point() { Coords = new Coords(3, 1) });
            regularPwd_3.Add(new Point() { Coords = new Coords(3, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(3, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(4, 2) });
            regularPwd_3.Add(new Point() { Coords = new Coords(4, 3) });

            regularPwd_1_reversed = new SimplePassword();
            foreach (IPoint p in regularPwd_1.Points.Reverse())
            {
                regularPwd_1_reversed.Add(p);
            }
        }
    }
}
