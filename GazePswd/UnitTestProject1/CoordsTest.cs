﻿using System;
using GazePswd;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GazePasswordTests
{


    [TestClass]
    public class PointTest
    {
        [TestMethod]
        public void Distance_symmetry()
        {
            Coords p1 = new Coords(5, 18);
            Coords p2 = new Coords(3, 4);

            double p1_p2 = p1.DistanceTo(p2);
            double p2_p1 = p2.DistanceTo(p1);

            Assert.AreEqual(p1_p2, p2_p1);
        }

        [TestMethod]
        public void Distance_symmetry_unit()
        {
            Coords p1 = new Coords(1, 1);
            Coords p2 = new Coords(1, 1);

            double p1_p2 = p1.DistanceTo(p2);
            double p2_p1 = p2.DistanceTo(p1);

            Assert.AreEqual(p1_p2, p2_p1);
        }

        [TestMethod]
        public void Distance_to_equivalent_unit()
        {

            Coords p1 = new Coords(1, 1);
            Coords p2 = new Coords(1, 1);
            Assert.AreEqual(0d, p1.DistanceTo(p2));
        }

        [TestMethod]
        public void Distance_to_equivalent()
        {
            Coords p1 = new Coords(3, 7);
            Coords p2 = new Coords(3, 7);
            Assert.AreEqual(0d, p1.DistanceTo(p2));
        }

        [TestMethod]
        public void Distance_to_self_unit()
        {
            Coords p1 = new Coords(1, 1);
            Assert.AreEqual(0d, p1.DistanceTo(p1));
        }

        [TestMethod]
        public void Distance_to_self()
        {
            Coords p1 = new Coords(5, 6);
            Assert.AreEqual(0d, p1.DistanceTo(p1));
        }
        
        [TestMethod]
        public void Distance_1()
        {
            int x1 = 8;
            int y1 = 3;
            int x2 = 5;
            int y2 = 2;

            double expected_distance = 3.16227766;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(x2, y2);

            double diff = Math.Abs(expected_distance - p1.DistanceTo(p2));
            Assert.IsTrue(diff < 1E-8, ""+ diff);
        }

        [TestMethod]
        public void Distance_2()
        {
            int x1 =2;
            int y1 = 3;
            int x2 = 2;
            int y2 = 5;

            double expected_distance = 2;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(x2, y2);

            Assert.AreEqual(expected_distance, p1.DistanceTo(p2));
        }

        [TestMethod]
        public void Distance_3()
        {
            int x1 = 2;
            int y1 = 3;
            int x2 = 6;
            int y2 = 3;

            double expected_distance = 4;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(x2, y2);

            Assert.AreEqual(expected_distance, p1.DistanceTo(p2));
        }

        [TestMethod]
        public void ToTuple()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p = new Coords(x1, y1);
            Tuple<int, int> t = p.ToTuple();

            Assert.AreEqual(x1, t.Item1);
            Assert.AreEqual(y1, t.Item2);
        }

        [TestMethod]
        public void ToString_1()
        {
            int x1 = 2;
            int y1 = 3;

            String expected_string = "(x=" + x1 + " y=" + y1 + ")";

            Coords p = new Coords(x1, y1);
            String p_string = p.ToString();

            Assert.AreEqual(expected_string, p_string);
        }

        [TestMethod]
        public void ToString_2()
        {
            int x1 = 19;
            int y1 = 9;

            String expected_string = "(x=" + x1 + " y=" + y1 + ")";

            Coords p = new Coords(x1, y1);
            String p_string = p.ToString();

            Assert.AreEqual(expected_string, p_string);
        }

        [TestMethod]
        public void Equals_self()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p = new Coords(x1, y1);

            Assert.AreEqual(p, p);
        }

        [TestMethod]
        public void Equals_equivalent()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(x1, y1);

            Assert.AreEqual(p1, p2);
        }

        [TestMethod]
        public void Not_Equals_null()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p1 = new Coords(x1, y1);

            Assert.AreNotEqual(p1, null);
            Assert.AreNotEqual(null, p1);
        }

        [TestMethod]
        public void Not_Equals_different_1()
        {
            int x1 = 2;
            int y1 = 3;
            int x2 = 5;
            int y2 = 3;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(x2, y2);

            Assert.AreNotEqual(p1, p2);
            Assert.AreNotEqual(p2, p1);
        }

        [TestMethod]
        public void Not_Equals_transposed()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(y1, x1);

            Assert.AreNotEqual(p1, p2);
            Assert.AreNotEqual(p2, p1);
        }

        [TestMethod]
        public void GetHashCode_equal()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p1 = new Coords(x1, y1);
            Coords p2= new Coords(x1, y1);

            Assert.AreEqual(p1.GetHashCode(), p2.GetHashCode());
        }

        [TestMethod]
        public void Not_GetHashCode_transposed()
        {
            int x1 = 2;
            int y1 = 3;

            Coords p1 = new Coords(x1, y1);
            Coords p2 = new Coords(y1, x1);

            Assert.AreNotEqual(p1.GetHashCode(), p2.GetHashCode());
        }
    }
}
