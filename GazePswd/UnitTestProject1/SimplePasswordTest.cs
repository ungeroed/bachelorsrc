﻿using System;
using GazePswd;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class SimplePasswordTest
    {
        [TestMethod]
        public void Add_to_empty()
        {
            SimplePassword pwd = new SimplePassword();
            Point p = new Point () { Coords = new Coords(1, 2) };

            Assert.AreEqual(0, pwd.Points.Count());
            pwd.Add(p);
            Assert.AreEqual(1, pwd.Points.Count());
            Assert.AreEqual(p.Coords, pwd.Points.First().Coords);
        }

        [TestMethod]
        public void Add_same()
        {
            SimplePassword pwd = new SimplePassword();
            Point p = new Point() { Coords = new Coords(1, 2) };

            Assert.AreEqual(0, pwd.Points.Count());
            pwd.Add(p);
            pwd.Add(p);
            Assert.AreEqual(2, pwd.Points.Count());
            Assert.AreEqual(p.Coords, pwd.Points.First().Coords);
            Assert.AreEqual(p.Coords, pwd.Points.ElementAt(1).Coords);
        }

        [TestMethod]
        public void Add_1()
        {
            SimplePassword pwd = new SimplePassword();
            Point p1 = new Point() { Coords = new Coords(1, 2) };
            Point p2 = new Point() { Coords = new Coords(3, 4) };

            pwd.Add(p1);
            pwd.Add(p2);
            Assert.AreEqual(2, pwd.Points.Count());
            Assert.AreEqual(p1.Coords, pwd.Points.First().Coords);
            Assert.AreEqual(p2.Coords, pwd.Points.ElementAt(1).Coords);
        }

        [TestMethod]
        public void Clone_is_equal()
        {
            SimplePassword pwd = new SimplePassword();
            Point p1 = new Point() { Coords = new Coords(1, 2) };
            Point p2 = new Point() { Coords = new Coords(3, 4) };
            Point p3 = new Point() { Coords = new Coords(5, 6) };

            pwd.Add(p1);
            pwd.Add(p2);
            pwd.Add(p3);

            SimplePassword pwd_clone = (SimplePassword) pwd.Clone();

            Assert.AreEqual(pwd.Points.Count(), pwd_clone.Points.Count());
            Assert.AreEqual(p1.Coords, pwd.Points.First().Coords);
            Assert.AreEqual(p2.Coords, pwd.Points.ElementAt(1).Coords);
            Assert.AreEqual(p3.Coords, pwd.Points.ElementAt(2).Coords);
        }

        [TestMethod]
        public void Clone_is_shallow()
        {
            SimplePassword pwd = new SimplePassword();
            Point p1 = new Point() { Coords = new Coords(1, 2) };
            Point p2 = new Point() { Coords = new Coords(3, 4) };
            Point p3 = new Point() { Coords = new Coords(5, 6) };

            pwd.Add(p1);
            pwd.Add(p2);
            pwd.Add(p3);

            SimplePassword pwd_clone = (SimplePassword)pwd.Clone();

            Assert.AreNotSame(pwd, pwd_clone);
            Assert.ReferenceEquals(p1, pwd.Points.First());
            Assert.ReferenceEquals(p2, pwd.Points.ElementAt(1));
            Assert.ReferenceEquals(p3, pwd.Points.ElementAt(2));
        }
    }
}
