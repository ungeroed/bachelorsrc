﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GazePswd;

namespace UnitTestProject1
{
    [TestClass]
    public class TimeBucketAnalyzeerTest
    {
        IPassword simplePwd_1;
        IPassword simplePwd_2;
        IPassword simplePwd_3;
        IPassword simplePwd_4;

        [TestMethod]
        public void NameTest()
        {
            TimeBucketAnalyzer tba = new TimeBucketAnalyzer(true);
            Assert.AreEqual("TimeBucketAnalyzer", tba.Name);
        }

        [TestMethod]
        public void Match_is_reflexive()
        {
            TimeBucketAnalyzer da = new TimeBucketAnalyzer(true);
            Assert.AreEqual(0d, da.Match((OriginalPswd)simplePwd_1, (AttemptPswd)simplePwd_1));
        }

        [TestMethod]
        public void Match_distance()
        {
            TimeBucketAnalyzer da = new TimeBucketAnalyzer(true);

            Assert.AreNotEqual(0d, da.Match((OriginalPswd)simplePwd_1, (AttemptPswd)simplePwd_2));
        }

        [TestMethod]
        public void Match_relative_half()
        {
            TimeBucketAnalyzer da = new TimeBucketAnalyzer(true);

            Assert.AreEqual(0d, da.Match((OriginalPswd)simplePwd_1, (AttemptPswd)simplePwd_3));
        }

        
        [TestMethod]
        public void Match_relative_double()
        {
            TimeBucketAnalyzer da = new TimeBucketAnalyzer(true);

            Assert.AreEqual(0d, da.Match((OriginalPswd)simplePwd_1, (AttemptPswd)simplePwd_4));
        }

        [TestMethod]
        public void Match_is_symmetric()
        {
            TimeBucketAnalyzer da = new TimeBucketAnalyzer(true);

            double pw1_pw2 = da.Match((OriginalPswd)simplePwd_1, (AttemptPswd)simplePwd_2);
            double pw2_pw1 = da.Match((OriginalPswd)simplePwd_2, (AttemptPswd)simplePwd_1);

            Assert.AreEqual(pw1_pw2, pw2_pw1);
        }

        [TestInitialize]
        public void setupPasswords()
        {
            DateTime reference = DateTime.Now;
            //Handwritten
            simplePwd_1 = new SimplePassword();
            simplePwd_1.Add(new Point() { Coords = new Coords(11, 6), Time = reference });
            simplePwd_1.Add(new Point() { Coords = new Coords(6, 7), Time = reference.AddMilliseconds(2000)});
            simplePwd_1.Add(new Point() { Coords = new Coords(10, 7), Time = reference.AddMilliseconds(4000)});
            simplePwd_1.Add(new Point() { Coords = new Coords(12, 7), Time = reference.AddMilliseconds(6000)});
            simplePwd_1.Add(new Point() { Coords = new Coords(12, 5), Time = reference.AddMilliseconds(8000)});

            //Handwritten
            simplePwd_2 = new SimplePassword();
            simplePwd_2.Add(new Point() { Coords = new Coords(11, 6), Time = reference });
            simplePwd_2.Add(new Point() { Coords = new Coords(6, 7), Time = reference.AddMilliseconds(1000)});
            simplePwd_2.Add(new Point() { Coords = new Coords(10, 7), Time = reference.AddMilliseconds(4000)});
            simplePwd_2.Add(new Point() { Coords = new Coords(12, 7), Time = reference.AddMilliseconds(9000)});
            simplePwd_2.Add(new Point() { Coords = new Coords(12, 5), Time = reference.AddMilliseconds(9001)});

            //Handwritten
            simplePwd_3 = new SimplePassword();
            simplePwd_3.Add(new Point() { Coords = new Coords(11, 6), Time = reference });
            simplePwd_3.Add(new Point() { Coords = new Coords(6, 7), Time = reference.AddMilliseconds(1000) });
            simplePwd_3.Add(new Point() { Coords = new Coords(10, 7), Time = reference.AddMilliseconds(2000) });
            simplePwd_3.Add(new Point() { Coords = new Coords(12, 7), Time = reference.AddMilliseconds(3000) });
            simplePwd_3.Add(new Point() { Coords = new Coords(12, 5), Time = reference.AddMilliseconds(4000) });
         
            //handwritten
            simplePwd_4 = new SimplePassword();
            simplePwd_4.Add(new Point() { Coords = new Coords(11, 6), Time = reference });
            simplePwd_4.Add(new Point() { Coords = new Coords(6, 7), Time = reference.AddMilliseconds(4000) });
            simplePwd_4.Add(new Point() { Coords = new Coords(10, 7), Time = reference.AddMilliseconds(8000) });
            simplePwd_4.Add(new Point() { Coords = new Coords(12, 7), Time = reference.AddMilliseconds(12000) });
            simplePwd_4.Add(new Point() { Coords = new Coords(12, 5), Time = reference.AddMilliseconds(16000) });
        
        
        }
    }
}
