﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class AttemptStopper
    {
        public static AttemptStopper Instance;

        private bool started = false;
        private DateTime startTime;

        private List<AttemptPswd> list;

        //Calculated limits
 
        int numberOfPswdPoints = int.MaxValue;
        int count = 0;

        //Delegate method for pub sub pattern
        public Action<Access> handler;

        /// <summary>
        /// Static constructor for singleton pattern.
        /// </summary>
        static AttemptStopper()
        {
            Instance = new AttemptStopper();
        }

        /// <summary>
        /// Private constructor for singleton pattern
        /// </summary>
        private AttemptStopper() 
        {
            list = new List<AttemptPswd>();
        }

        public void Analyze(OriginalPswd orig, AttemptPswd attempt)
        {
           


            list.Add(attempt);
            bool stop = false;

            //if dwell tim eis enabled we just check for number of points.
            if (attempt.Points.Count() >= orig.Points.Count() * 2)
                stop = true;
            //If this is the first password that is tested
            if (!started)
            {
                startTime = DateTime.Now;
                
                numberOfPswdPoints = 2 * orig.Points.Count();
            }

           
            // for all attempts: lenght of attempt over threshold. Only performed after minimum 4 attempts.
            if(list.Count() > 3)
            {
                if (list.All(s => s.Points.Count() >= numberOfPswdPoints))
                {
                    Console.Out.WriteLine("number of points in all paswds in list over limit");
                    stop = true;
                }
            }

            //totalt number of pswd attempts over limit
            if (count >= Settings.numberOfpswdsPerAttempt)
            {
                stop = true;
                Console.Out.WriteLine("number of total attempts too many");
            }

            if (stop)
            {
                reset();
                if (handler != null)
                {
                    handler.Invoke(Access.Denied);
                }
            }
            
        }


      
        /// <summary>
        /// removes all currently stored passwords
        /// </summary>
        public void Flush()
        {
            list = new List<AttemptPswd>();
        }

        private void reset()
        {
            started = false;
            list = new List<AttemptPswd>();
            count = 0;
        }
    }
}
