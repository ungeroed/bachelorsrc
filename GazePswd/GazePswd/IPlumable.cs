﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    interface IPlumable{}

    interface IPlumable<T,U> : IPlumable
    {
        void Reader(IPipe<T> connection);

        void Writer(IPipe<U> connection);
    }
}
