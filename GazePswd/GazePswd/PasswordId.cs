﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GazePswd
{
    /// <summary>
    /// A username/password combination
    /// </summary>
    class PasswordId
    {
        public PasswordId(String userName, String passwordId)
        {
            this.UserName = userName;
            this.PasswordName = passwordId;
        }

        public String UserName
        {
            get;
            protected set;
        }

        public String PasswordName
        {
            get;
            protected set;
        }

        public override string ToString()
        {
            return UserName + Path.DirectorySeparatorChar + PasswordName;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            PasswordId asPwdid = obj as PasswordId;
            if ((System.Object)asPwdid == null)
            {
                return false;
            }
            return asPwdid.UserName == UserName && asPwdid.PasswordName == PasswordName;
        }

        public override int GetHashCode()
        {
            return (UserName + PasswordName).GetHashCode();
        }
    }
}
