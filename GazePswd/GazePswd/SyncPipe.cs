﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GazePswd
{
    /// <summary>
    /// This clas is a synchronous pipe that is used when a password is to be stored.
    /// It implements interfaces for receiving Points.
    /// </summary>
    class SyncPipe<T> : IPipe<T>
    {
           private Action<T> handler;

        void IPipe<T>.Write(T element)
        {
            
            if(handler != null)
                handler.Invoke(element);
        }

        void IPipe<T>.Subscribe(Action<T> callback)
        {
            handler += callback;
        }

        void IPipe<T>.Flush()
        {
            handler = null;
        }
        
       


    }
}
