﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class TotalTimeAnalyzer : IAnalyzer
    {
        public double Match(OriginalPswd orig, AttemptPswd attempt)
        {
            double t1 = (attempt.Points.Reverse().ElementAt(0).Time - attempt.Points.ElementAt(0).Time).TotalMilliseconds;
            double t2 = (orig.Points.Reverse().ElementAt(0).Time - orig.Points.ElementAt(0).Time).TotalMilliseconds;
            return Math.Abs(t2 - t1)/t2;
        }

        public String Name
        {
            get { return "Total time deviation: "; }
        }

    }
}
