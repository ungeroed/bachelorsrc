﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GazePswd
{
    public interface IPoint
    {
        //internal coordinate representation (i.preBounding. normalised)
        Coords Coords{get; set;}
        //startTime of capture
        DateTime Time { get; set; }
        //absolute coordinates in image.
        Coords PixelCoords { get; set; }
        //size of internal coordinate system
        Coords CoordAmount { get; set; }
        //size of absolute cordinatesystem
        Coords PixelBound { get; set; }
        //screencoords
        Coords ScreenCoords { get; set; }

    }
}
