﻿using System;
using System.Collections.Generic;

namespace GazePswd
{
    
    interface IPersist
    {
        IPassword Read(PasswordId id);
        
        IEnumerable<IPassword> ReadAttempt(PasswordId id, String attemptName, LoginMode mode);

        PasswordId Write(PasswordId id, IPassword pswd);

        String WriteAttempt(PasswordId id, IPassword pswd, String attemptName, LoginMode mode);

        IEnumerable<String> List();

        IEnumerable<PasswordId> List(String user);

        IEnumerable<String> ListAttempts(PasswordId id, LoginMode mode);

        //horribly ad-hoc, but startTime..
        void WriteStrengthData(PasswordId id, String strengths);

        void WritePrecullData(PasswordId id, IPassword pswd, String attemptName, LoginMode mode);

        bool exists(PasswordId id);

        void DeletePswd(PasswordId id);
    }

    enum LoginMode { 
        Attack, Attempt 
    };
}
