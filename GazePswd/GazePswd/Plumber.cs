﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace GazePswd
{
    class Plumber
    {
        MainView view;
        //Interaction with user goes through this.
        ConsoleUi console = ConsoleUi.Instance;

        //Main filter classes
        IPlumable<IPoint, IPoint> tracker;
        IPlumable<IPoint, IPoint> trackingDataValidator;
        IPlumable<IPoint, IPoint> pathfinder;
        IPlumable<IPoint, IPassword> pwIdentifier;
        IPlumable<IPassword, string> validator;
        IPlumable<IPoint,IPassword> persistance;
        IPlumable<IPassword, IPassword> attemptSaver;
        IPlumable<IPassword, string> setupPasswordAnalyzer;
        //temporarily removed for refactor purposes
        IPlumable<string,string> strengthLogger;
        IPlumable<string, string> consoleLogger;

        //List of all pipes kept here so they can be flushed easily to empty remaining points.
        List<IPipe<IPoint>> pointPipes = new List<IPipe<IPoint>>();
        List<IPipe<IPassword>> pswdPipes = new List<IPipe<IPassword>>();
        List<IPipe<string>> stringPipes = new List<IPipe<string>>();

        //The current password identifier
        private PasswordId currentPassword;
        //Current mode of attempts
        private LoginMode loginMode;
        //Use the rex Eyetracker
        bool useEyeTracker;
        
        //the name of the current attempt
        private string attemptName;

        //True if a menu is currently loaded
        bool menu = false;

        //to resume the correct menu
        CurrentMenu currentMenu;

        //timer thread and control
        Thread timerThread;
        //true if the timer has been startet
        private bool timerStarted = false;

        //attempt currently in progress, for the timer to alert or not
        public static bool attemptInProgress = false;

        /// <summary>
        /// Hotswapping semi-controller class
        /// </summary>
        /// <param name="v"></param>
        public Plumber(MainView v)
        {       
            view = v;
            instantiate();
            selectTracker();
            selectMode();
                           
        }

        /// <summary>
        /// Menu function that lets you select which mode to run the program in. Playmode is standard.
        /// </summary>
        private void selectMode()
        {
            Console.Clear();
            String s = console.Selection(new List<string>() { "Test Mode", "Play Mode" }, "feature:", false);
            switch (s)
            {
                case "Test Mode":
                    String test = console.Selection(new List<string>() { "Test 1", "Test 2", "Test 3", "Test 4", "Test 5" }, "feature:", false);
                    switch (test)
                    {
                        case "Test 1":
                            startTest1();
                            break;
                        case "Test 2":
                            startTest2();
                            break;
                        case "Test 3":
                            startTest3();
                            break;
                        case "Test 4":
                            startTest4();
                            break;
                        case "Test 5":
                            startTest5();
                            break;
                    }
                    break;
                case "Play Mode":
                    startPlayMode();
                    return;
            }
        }

        /// <summary>
        /// Lets you select and instantiate the tracker
        /// </summary>
        private void selectTracker()
        {
            Console.Clear();
            String s = console.Selection(new List<string>() { "Tobii REX Eye Tracker", "Mouse tracker" }, "Tracker", false);

            if (s == "Tobii REX Eye Tracker")
            {
                Console.Out.WriteLine("Using Tobii Eye Tracker.");
                if (tracker == null)
                {
                    Thread t1 = new Thread(() => tracker = new TrackWrapper());
                    t1.IsBackground = true;
                    t1.Start();
                }
                useEyeTracker = true;
            }
            else
            {
                console.Tell("Using Mouse Tracker.");
                tracker = MouseTracker.Instance;
                view.EnableMouseTracker();
                useEyeTracker = false;
            }
        }

//----------------------------- Runtime Modes --------------------------------------------
        private void startPlayMode()
        {
            view.nextImage();
            currentPassword = new PasswordId("PlayMode", getUpdatedPassword());
            loginMode = LoginMode.Attempt;
            attemptName = "PlayModeAttempt";
            Normalmode();
            playMenu();
        }

        /// <summary>
        /// Passwords with restrictions has less saliency test
        /// first make unrestricted after that make restricted
        /// </summary>
        private void startTest1()
        {
            currentPassword = new PasswordId("Test_1", getUpdatedPassword());
            loginMode = LoginMode.Attempt;
            attemptName = "Test1_Attempt";
            Normalmode();
            playMenu();
        }

        /// <summary>
        /// Is it possible to LOGIN with saliency bounds?
        /// </summary>
        private void startTest2()
        {
            view.nextImage();
            currentPassword = new PasswordId("Test_2", getUpdatedPassword());
            loginMode = LoginMode.Attempt;
            attemptName = "Test2_Attempt";
            Normalmode();
            playMenu();
        }

        /// <summary>
        /// Is it possible to HACK with saliency bounds?
        /// show video of successfull login attempt and let the test users try to login.
        /// </summary>
        private void startTest3()
        {
            currentPassword = new PasswordId("Test_3", "Test_3_Pswd1");
            loginMode = LoginMode.Attack;
            attemptName = "Test3_Attempt";
            Normalmode();
            Test3Menu();
        }

        /// <summary>
        /// Fluent passwords are harder to shoulder surf than dwell startTime selected ones.
        /// </summary>
        private void startTest4()
        {
            //disable saliency for this one
            Settings.useRestrictions = false;
            currentPassword = new PasswordId("Test_4", "");
            loginMode = LoginMode.Attack;
            attemptName = "Test4_Attempt_No_preculling";
            Normalmode();
            playMenu();
        }

        /// <summary>
        /// Given the path of the password, is it possible to hack the startTime analyzer?
        /// </summary>
        private void startTest5()
        {
            //disable saliency for this one
            Settings.useRestrictions = false;
            //disable preculling
            Settings.preCull = false;
            //enable only time analyzer
            Settings.AnalyzerAggregate = ValidationMethods.relativeTimeBucket;
            loginMode = LoginMode.Attack;
            currentPassword = new PasswordId("Test_5", "Test5_1");
            
            Normalmode();

            testerName = console.Ask("Please enter name of test person:");
            test5Menu();
            
        }

//---------------------------- Runtime menus ---------------------------------------------
        string testerName;
        /// <summary>
        /// Menu, specificly for test 5. This menu contains only options viable for test 5.
        /// </summary>
        private void test5Menu()
        {
            currentMenu = CurrentMenu.Test5;
            menu = true;
            IPassword p;
            String s = console.Selection(new List<string>() { "First", "Second", "Third", "Attempt Login", "Quit" }, "Test 5 event:", false);
            switch (s)
            { 
                case "First":
                    currentPassword = new PasswordId("Test_5", "Test5_1");
                    loadPassword();
                    Console.Out.WriteLine("Password loaded - Press any key to draw password.");
                    Console.ReadKey();
                    p = ((DiskPersistWrapper<IPoint>)persistance).Restore(currentPassword);
                    view.DrawPassword(p, 1);
                    break;
                case "Second":
                    currentPassword = new PasswordId("Test_5", "Test5_2");
                    loadPassword();
                    Console.Out.WriteLine("Password loaded - Press any key to draw password.");
                    Console.ReadKey();
                    p = ((DiskPersistWrapper<IPoint>)persistance).Restore(currentPassword);
                    view.DrawPassword(p, 1);
                    break;
                case "Third":
                    currentPassword = new PasswordId("Test_5", "Test5_3");
                    loadPassword();
                    Console.Out.WriteLine("Password loaded - Press any key to draw password.");
                    Console.ReadKey();
                    p = ((DiskPersistWrapper<IPoint>)persistance).Restore(currentPassword);
                    view.DrawPassword(p, 1);
                    break;
                case "Attempt Login":
                    startAttempt();
                    return;
                case "Quit":
                    quit();
                    break;
             }
             test5Menu();
            
        }

        /// <summary>
        /// Simple menu for Play Mode. Has all viable options for playmode.
        /// </summary>
        private void playMenu()
        {
            currentMenu = CurrentMenu.Play;
            menu = true;
            String s = console.Selection(new List<string>() { "Record Password", "Attempt Login", "Toggle Gaze", "Clear Image", "Process Attempts", "Advanced Features", "Quit" }, "Play Mode event:", false);
            switch (s)
            {
                case "Record Password":
                    setupForRecordMode();
                    break;
                case "Attempt Login":
                    startAttempt();
                    return;
                case "Toggle Gaze":
                    view.ToggleGaze();
                    break;
                case "Clear Image":
                    view.ClearImage();
                    break;
                case "Process Attempts":
                    
                    ((PswdIdentifier)pwIdentifier).flush();
                    IEnumerable<IPassword> att = getAttempts();
                    if (att == null)
                        break;
                    setPointSelection();
                    foreach (IPassword attempt in att)
                    {
                        IPipe<IPassword> tempPipe = new SyncPipe<IPassword>(); // pretty sure this is a memory leak..
                        validator.Reader(tempPipe);
                        tempPipe.Write(attempt);
                    }
                    menu = false;
                    return;
                case "Advanced Features":
                    advancedMenu();
                    break;
                case "Quit":
                    quit();
                    break;
            }
            playMenu();
        }

        /// <summary>
        /// Simple menu for test number 3.
        /// </summary>
        int test3Counter = 0;
        bool set = false;
        private void Test3Menu()
        {

            string[] arr = new string[]{"Test_3_Pswd2", "Test_3_Pswd3"};
            currentMenu = CurrentMenu.Test3;
            menu = true;
            String s = console.Selection(new List<string>() {"Attempt Login", "Next Password", "Quit" }, "Play Mode event:", false);
            switch (s)
            {
                case "Attempt Login":
                    if (!set)
                    {
                        loadPassword();
                        set = true;
                    }
                    ((AttemptSaver)attemptSaver).EnableSaveAttempts(currentPassword, attemptName, loginMode);
                    ((ITracker)tracker).TrackingEnabled(true);
                    menu = false;
                    return;
                case "Next Password":
                    view.ShowLogo();
                    if (test3Counter == 2)
                    {
                        break;
                    }
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                   
                    currentPassword = new PasswordId(currentPassword.UserName, arr[test3Counter]);
                    test3Counter++;
                    loadPassword();
                     ((AttemptSaver)attemptSaver).EnableSaveAttempts(currentPassword, attemptName, loginMode);
                    ((ITracker)tracker).TrackingEnabled(true);
                    menu = false;
                    return;
                case "Quit":
                    quit();
                    break;
            }
            playMenu();
        }

       
        /// <summary>
        /// Simple quit confimation selection. Makes sure the program doesnt exit if you accidently hits "quit"
        /// </summary>
        private void quit()
        {
            String s = console.Selection(new List<string>() { "Yes", "No" }, "if you want to quit?", false);
            switch (s)
            {
                case "Yes":
                    Application.Exit();
                    return;
                case "No":
                    break;
            }
        }

        /// <summary>
        /// This method is run after each attempt finishes. 
        /// It notifies the user with a sound that he is either accessed or denied.
        /// If he is denied on saliency this will be said as well.
        /// </summary>
        /// <param name="acc"></param>
        private void handleAccessResults(Access acc)
        {
            attemptInProgress = false;
            ((ITracker)tracker).TrackingEnabled(false);
            if (acc == Access.Granted)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer("access.wav");
                player.Play();
                
            }
            if (acc == Access.Denied)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer("denied.wav");
                player.Play();
                
            }
            if (acc == Access.SaliencyDenied)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer("saliency.wav");
                player.Play();   
            }
            instantiate();
            loadPassword();
            Normalmode();
            if (!menu)
            {
                resumeMenu();
            }
        }

        /// <summary>
        /// resumes the last active menu
        /// This makes sure the correct menu is started again after an attempt
        /// </summary>
        private void resumeMenu()
        {
            switch (currentMenu)
            {
                case CurrentMenu.Play:
                    playMenu();
                    break;
                case CurrentMenu.Test1:
                   //test1Menu();
                    break;
                case CurrentMenu.Test3:
                    Test3Menu();
                    break;
                case CurrentMenu.Test4:
                    playMenu();
                    break;
                case CurrentMenu.Test5:
                    test5Menu();
                    break;

            }
        }

        /// <summary>
        /// Advanced runtime menu.
        /// This menu holds all possible options available in the program.
        /// </summary>
        private void advancedMenu()
        {
            String s = console.Selection(new List<string>() { "Choose Point Selection","Record new Password", 
                "Start Tracking", "Stop Tracking", "Show grid", "Draw Password", "Restart", "Draw Password Quadrants", 
                "Draw Bounding Box", "Draw Attempts", "Next image", "Show saliency","Disable Saliency Restrictions", "Show pswd saliency amount" ,"Load Password", "Back"}, "Play Mode event:", false);
            switch (s)
            {
                case "Choose Point Selection":
                    setPointSelection();
                    break;
                case "Record new Password":
                    setupForRecordMode();
                    break;
                case "Start Tracking":
                    ((AttemptSaver)attemptSaver).EnableSaveAttempts(currentPassword, attemptName, loginMode);
                    console.Tell("Tracking enabled");
                    ((ITracker)tracker).TrackingEnabled(true);
                    break;
                case "Stop Tracking":
                    ((ITracker)tracker).TrackingEnabled(false);
                    Console.Out.WriteLine("Tracking disabled ");
                    break;
                case "Show grid":
                    view.drawGrid();
                    break;
                case "Draw Password":
                    DiskPersistWrapper<IPoint> per = (DiskPersistWrapper<IPoint>)persistance;
                    IPassword p = per.Restore(currentPassword);
                    view.DrawPassword(p, 1);
                    break;
                case "Restart":
                    Console.Clear();
                    //restart();
                    break;
                case "Draw Password Quadrants":
                    DiskPersistWrapper<IPoint> pers = (DiskPersistWrapper<IPoint>)persistance;
                    IPassword ps = pers.Restore(currentPassword);
                    view.DrawQuadrants(ps);
                    break;
                case "Show pswd saliency amount":
                    IPassword myps = ((DiskPersistWrapper<IPoint>)persistance).Restore(currentPassword);
                    view.DrawText(SaliencyAnalyzer.Instance.PwdSaliencyToAvg(myps).ToString(), 2);
                    break;
                case "Draw Bounding Box":
                    DiskPersistWrapper<IPoint> persis = (DiskPersistWrapper<IPoint>)persistance;
                    IPassword psw = persis.Restore(currentPassword);
                    BoundingBoxer boxer = new BoundingBoxer();
                    view.DrawBox(boxer.GetBoundingBox(psw));
                    break;
                case "Draw Attempts":
                    int color = 2;
                    IEnumerable<IPassword> attempts = getAttempts();
                    if (attempts == null)
                        break;
                    foreach (IPassword attempt in attempts)
                    {
                        view.DrawPassword(attempt, color);
                        console.Ask("Press RETURN key to show next attempt");
                    }
                    break;
                case "Next image":
                    view.nextImage();
                    break;
                case "Show saliency":
                    ((MainView)view).ShowSaliency();
                    break;
                case "Disable Saliency Restrictions":
                    Settings.useRestrictions = false;
                    break;
                case "Load Password":
                    selectAnotherPassword();
                    break;
                case "Back":
                    return;
            }

        }
//-----------------------------------------------------------------------------------------
        /// <summary>
        /// This method starts an attempt. It starts a timer thread that can stop the attempt after some time. 
        /// It also starts the actual tracking.
        /// </summary>
        private void startAttempt()
        {
            ((AttemptSaver)attemptSaver).EnableSaveAttempts(currentPassword, attemptName, loginMode);
            ((ITracker)tracker).TrackingEnabled(true);
            menu = false;

            //If the timer has not been startet yet, start it
            if (!timerStarted)
            {
                timerThread = new Thread(() => startTimer());
                timerThread.IsBackground = true;
                timerThread.Start();
                timerStarted = true;
            }

        }

        /// <summary>
        /// starts attempttimer. starts the timer. 
        /// This is always started from another thread so it doesnt block other threads. 
        /// </summary>
        private void startTimer()
        {
            DateTime startTime = DateTime.Now;
            IPassword pass = ((DiskPersistWrapper<IPoint>)persistance).Restore(currentPassword);
            // K * Total startTime over threshold plus 3 sec for startup troubles
            int sleepTime = (int)(2.5 * (pass.Points.Last().Time - pass.Points.First().Time).TotalMilliseconds);
            Thread.Sleep(sleepTime);
            if (attemptInProgress)
            {
                timerStarted = false;
                AttemptStopper.Instance.handler.Invoke(Access.Denied);
            }
            timerStarted = false;
        }

        /// <summary>
        /// Instantiates all plumables.
        /// This method is run at program start.
        /// </summary>
        private void instantiate()
        {
            //Instantiate all Iplumables         
            trackingDataValidator = new TrackingDataValidator(view);
            pathfinder = new Pathfinder();
            pwIdentifier = new PswdIdentifier();
            validator = new Validator();
            persistance = new DiskPersistWrapper<IPoint>();
            attemptSaver = new AttemptSaver();
            setupPasswordAnalyzer = new SetupPswdAnalyzer();
            strengthLogger = new StrengthLogger();
            consoleLogger = new ConsoleLogger();
            AttemptStopper.Instance.Flush();
            AttemptStopper.Instance.handler = handleAccessResults;
        }

        /// <summary>
        /// loads another password;
        /// This method lets u type a name of another user to load a password from, or select it from a list.
        /// </summary>
        private void selectAnotherPassword()
        {
            string user = console.Selection(((DiskPersistWrapper<IPoint>)persistance).List().ToList(), "a user to load a password from:", false);
            string pswd = console.Selection(((DiskPersistWrapper<IPoint>)persistance).ListPswds(user).ToList(), "password name", true);
            currentPassword = new PasswordId(user,pswd.Split('\\')[1]);
            loadPassword();
        }


        /// <summary>
        /// Lets you select between dwell startTime or normal mode
        /// This menu lets ud select between point selection mode. 
        /// </summary>
        private void setPointSelection()
        {
            string answer = console.Selection(new List<string>() {"Use Dwell time point selection","Use fluent point selection" }, "Point Selection Mode", false);
            switch(answer){
                case "Use Dwell time point selection":
                    Settings.UseDwell = true;
                    break;
                case "Use fluent point selection":
                    Settings.UseDwell = false;
                    break;
            }
            currentPassword = new PasswordId(currentPassword.UserName, getUpdatedPassword());
        }
      
        /// <summary>
        /// This method lets you select a username.
        /// If new is selected from the menu, you can enter a new name in the console. 
        /// </summary>
        /// <returns></returns>
        private String getUsername()
        {
            
            //This can be effectivitized, as list() function not optimized for this usage. 
            string answer = console.Selection(((DiskPersistWrapper<IPoint>)persistance).List().ToList(), "Username", true);
            //or list usernames
            if (answer == "New")
            {
                answer = console.Ask("Please enter the username: ");
                return answer;
            }
            else
            {
                return answer;
            }
        }

        /// <summary>
        /// Sets the password name based on point selection mode, and restrictions
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        private string getUpdatedPassword()
        {
            return ((Settings.useRestrictions) ? "Restricted" : "Unrestricted") + "_" + ((Settings.UseDwell) ? "DwellTime" : "Fluent");
            
        }
        
        /// <summary>
        /// returns the password name base on the supplied id
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public string getPasswordName(String username)
        {
            //get specific password under user
            String answer = console.Selection(((DiskPersistWrapper<IPoint>)persistance).ListPswds(username).ToList(), "password name", true);

            //or list passwords
            if (answer == "New")
            {
                answer = console.Ask("Please enter a password name: ");
            }
            else
            {
                answer = answer.Split('\\')[1];
            }
            return answer;          
        }

        /// <summary>
        /// This method returns the name of an attempt you wish to load.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private string getAttemptName(PasswordId id, LoginMode mode)
        {
            String attemptMode = mode.ToString();
            String attemptName = console.Selection(((DiskPersistWrapper<IPoint>)persistance).ListAttempts(id, mode).ToList(), "select name of the " + attemptMode, true);
            if (attemptName == "New")
            {
                Console.Clear();
                attemptName = console.Ask("Please enter a name of this "+ attemptMode +":");
            }
            return attemptName;
        }

        /// <summary>
        /// This method lets you selct login ode. Either attack or attempt.
        /// </summary>
        /// <returns></returns>
        private LoginMode getLoginMode()
        {
            String mode = console.Selection(new List<string>() {"Attack","Attempt"}, "login mode:", false);
            LoginMode m = LoginMode.Attempt;
            if (mode == "Attack")
                m = LoginMode.Attack;
            return m;
      
        }

        /// <summary>
        /// Returns a new attempt name for the current sessions.
        /// note these will overwrite if the same username is used after shutdown
        /// </summary>
        int currentAttemptNumber = 0;
        private string newAttemptName()
        {
            currentAttemptNumber++;
            return testerName + "_attempt_" + currentAttemptNumber; 
        }

        /// <summary>
        /// This method loads a password based on the current set username.
        /// </summary>
        private void loadPassword()
        {
            
            DiskPersistWrapper<IPoint> p = (DiskPersistWrapper<IPoint>)persistance;
            IPassword pwd = p.Restore(currentPassword);
            if (pwd == null)
                return;
            Settings.QuadrantAmountX = pwd.Points.First().CoordAmount.X;
            Settings.QuadrantAmountY = pwd.Points.ElementAt(0).CoordAmount.Y;
            view.SetPswdImg(pwd);
            //This is threaded to avoid small lag in user experience
            Thread t = new Thread(() => SaliencyAnalyzer.Instance.CalculateSalMap(pwd.ImgPath));
            t.IsBackground = true;
            t.Start();
            
            Validator.SetPswd(pwd);
            ((PswdIdentifier)pwIdentifier).SetPswd(currentPassword, pwd, attemptName, loginMode);
        }

        /// <summary>
        /// Retrieves user identification
        /// </summary>
        private void Initialize()
        {
            setPointSelection();
            String userName = getUsername();
            String password = getUpdatedPassword();
            currentPassword = new PasswordId(userName, password);
            loginMode = getLoginMode();
            attemptName = getAttemptName(currentPassword, loginMode);
            ((StrengthLogger)strengthLogger).SetUser(currentPassword);
            DiskPersistWrapper<IPoint> p = (DiskPersistWrapper<IPoint>)persistance;

            //loads the password from persistance if it exists
            if (p.exists(currentPassword))
            {
                loadPassword();
                Normalmode();
            }
            else
            {
                setupForRecordMode();
            }
        }

        /// <summary>
        /// Lets the user choose between images to use for password
        /// </summary>
        /// <returns></returns>
        private void selectImage()
        {
            view.FreezeImage(false);
            Console.Out.WriteLine("Press any key to select another image, press ENTER to accept!");
            while (Console.ReadKey().Key != ConsoleKey.Enter)
            {
                view.nextImage();
            }
            view.FreezeImage(true);
        }

        /// <summary>
        /// Initializes setupForRecordMode.
        /// THis method is run every time a new password is recorded. 
        /// lets you select restricted or unrestricted.
        /// It initialized synchronous pipes to make sure all points are saved.
        /// </summary>
        private void setupForRecordMode()
        {          
            flush();
            String restrict = console.Selection(new List<string>() { "Record Unrestricted", "Record with restrictions", "Cancel" }, "Recording mode:", false);
            switch(restrict)
            {
                case "Record Unrestricted":
                    Settings.useRestrictions = false;
                    break;
                case "Record with restrictions":
                    Settings.useRestrictions = true;
                    break;
                case "Cancel":
                    return;
            }
            currentPassword = new PasswordId(currentPassword.UserName, getUpdatedPassword());
            //Tracker to Tracker analyzer. 
            //This is async now, so we dont get blocking calls and miss tracking data. 
            IPipe<IPoint> trackerToTrackerDataValidatorLink = new AsyncPipe<IPoint>();
            IPipe<IPoint> trackerDataValidatorToPathfinderLink = new SyncPipe<IPoint>();
            
            //Pathfinder to pswdIdentifier
            IPipe<IPoint> pathfinderToPersistanceLink = new SyncPipe<IPoint>();
            
            //persistance to setupPswdAnalyzer
            IPipe<IPassword> link3 = new SyncPipe<IPassword>();

            //password analyzer to logger
            IPipe<string> link4 = new AsyncPipe<string>();
            ((ITracker)tracker).TrackingEnabled(false);      

            if (useEyeTracker)
            {
                tracker.Writer(trackerToTrackerDataValidatorLink);
                trackingDataValidator.Reader(trackerToTrackerDataValidatorLink);
                trackingDataValidator.Writer(trackerDataValidatorToPathfinderLink);
                pathfinder.Reader(trackerDataValidatorToPathfinderLink);
            }
            else
            {
                tracker.Writer(trackerDataValidatorToPathfinderLink);
                pathfinder.Reader(trackerDataValidatorToPathfinderLink);
            }          
            pathfinder.Writer(pathfinderToPersistanceLink);
            persistance.Reader(pathfinderToPersistanceLink);
            persistance.Writer(link3);
            setupPasswordAnalyzer.Reader(link3);
            setupPasswordAnalyzer.Writer(link4);
            strengthLogger.Reader(link4);
            consoleLogger.Reader(link4);

            //adding pipes to collection for easy flushing
            pointPipes.Add(trackerToTrackerDataValidatorLink);
            pointPipes.Add(trackerDataValidatorToPathfinderLink);
            pointPipes.Add(pathfinderToPersistanceLink);
            pswdPipes.Add(link3);
            stringPipes.Add(link4);
            
            //select the image
            selectImage();

            //prepare saliency map, threaded so it wont hold up UI
            Settings.QuadrantAmountY = (int) (view.BackgroundImage.Height / (view.BackgroundImage.Width/Settings.QuadrantAmountX));
            Thread salPrepare = new Thread(() => SaliencyAnalyzer.Instance.CalculateSalMap(view.CurrentImg));
            salPrepare.IsBackground = true;
            salPrepare.Start();

            //record the image
            recordPswd();

            //after record load the pswd TODO: select correct pswd??
            loadPassword();
            Normalmode();
        }

        /// <summary>
        /// loops until a correct pswd is recorded
        /// </summary>
        private void recordPswd()
        {
            ((SetupPswdAnalyzer)setupPasswordAnalyzer).ResetApproved();
            while (!((SetupPswdAnalyzer)setupPasswordAnalyzer).GetApproved())
            {

                ((DiskPersistWrapper<IPoint>)persistance).flush();
                Console.Out.WriteLine("Press SPACE to continue");
                while (Console.ReadKey().Key != ConsoleKey.Spacebar) { }

                console.Tell("Tracking startet");
                ((ITracker)tracker).TrackingEnabled(true);
                while (Console.ReadKey().Key != ConsoleKey.Spacebar) { }
                ((ITracker)tracker).TrackingEnabled(false);
                Console.Out.WriteLine("tracking ended");
                
                //save attempt at creating pswd
                DiskPersistWrapper<IPoint> p = (DiskPersistWrapper<IPoint>)persistance;
                currentPassword = p.Save(currentPassword, view.CurrentImg);
                console.Tell("Password saved for ID: " + currentPassword);

                //If the recorded pswd is not good enough
                if (Settings.useRestrictions && !((SetupPswdAnalyzer)setupPasswordAnalyzer).GetApproved())
                {
                    //view.DrawText(((SetupPswdAnalyzer)setupPasswordAnalyzer).GetFindings(), 2);
                    string answer = console.Ask("Do you want to view saliency image? (y/n)");
                    if (answer == "y")
                    {
                        view.ShowSaliency();
                        view.DrawPassword(((SetupPswdAnalyzer)setupPasswordAnalyzer).GetPswd(), 1);

                        //message to admin
                        Console.Out.WriteLine("Press any key to continue");
                        Console.ReadKey();
                    }   
                }
                else
                {
                    view.DrawText("Password Recorded Successfully",1);
                    Console.Out.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    return;
                }
                
               
            }
        }

        /// <summary>
        /// Setting up pipes for normalmode.
        /// </summary>
        private void Normalmode()
        {
            ((ITracker)tracker).TrackingEnabled(false);
            flush();
            //Tracker ti trackAnakyzer
            IPipe<IPoint> tracker2Trackanalyzer = new AsyncPipe<IPoint>();
            //Trackanalyzer to pathfinder
            IPipe<IPoint> trackAnalyzer2Pathfinder = new AsyncPipe<IPoint>();
            //pathfinder to pswdIdentifier
            IPipe<IPoint> pathfinder2pswdIdentifier = new AsyncPipe<IPoint>();
            //pswdIdentifier to attemptSaver
            IPipe<IPassword> pswdIdentifier2attemptSaver = new AsyncPipe<IPassword>();
            //attemptsaver to validator
            IPipe<IPassword> attemptsaver2validator = new AsyncPipe<IPassword>();
            //validator to Loggers
            IPipe<string> validator2loggers = new AsyncPipe<string>();
            
            
            tracker.Writer(tracker2Trackanalyzer);
            if (useEyeTracker)
            {
                trackingDataValidator.Reader(tracker2Trackanalyzer);
                trackingDataValidator.Writer(trackAnalyzer2Pathfinder);
                pathfinder.Reader(trackAnalyzer2Pathfinder);
            }
            else
            {
                pathfinder.Reader(tracker2Trackanalyzer);
            }
            
            pathfinder.Writer(pathfinder2pswdIdentifier);
            pwIdentifier.Reader(pathfinder2pswdIdentifier);
            pwIdentifier.Writer(pswdIdentifier2attemptSaver);        
            attemptSaver.Reader(pswdIdentifier2attemptSaver);
            attemptSaver.Writer(attemptsaver2validator);
            validator.Reader(attemptsaver2validator);
            validator.Writer(validator2loggers);
            consoleLogger.Reader(validator2loggers);

            //Making sure pipes can be flushed here by adding them to the lists.
            pointPipes.Add(tracker2Trackanalyzer);
            pointPipes.Add(trackAnalyzer2Pathfinder);
            pointPipes.Add(pathfinder2pswdIdentifier);
            pswdPipes.Add(pswdIdentifier2attemptSaver);
            pswdPipes.Add(attemptsaver2validator);
            stringPipes.Add(validator2loggers);
        }

        /// <summary>
        /// Flushes all pipes empty all remaining points.
        /// </summary>
        private void flush()
        {
            foreach (IPipe<IPoint> pipe in pointPipes)
            {
                pipe.Flush();
            }
            foreach (IPipe<IPassword> pipe in pswdPipes)
            {
                pipe.Flush();
            }
            foreach (IPipe<string> pipe in stringPipes)
            {
                pipe.Flush();
            }
            pointPipes.Clear();
            pswdPipes.Clear();
            stringPipes.Clear();
            ((DiskPersistWrapper<IPoint>) persistance).flush();
            ((PswdIdentifier)pwIdentifier).flush();

        }

        /// <summary>
        /// Converts a string to a login mode.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private LoginMode? stringToLoginMode(String str) {
            switch (str.ToLower())
            {
                case "attack":
                    return LoginMode.Attack;
                case "attempt":
                    return LoginMode.Attempt;
                default:
                    return null;
            }
        }

        /// <summary>
        /// This menu lets u select attempts to load.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IPassword> getAttempts()
        {
            string answer = console.Selection(new List<string>() { "Attack", "Attempt" }, "mode to load from:", false);
            LoginMode modeNotNull = (LoginMode) stringToLoginMode(answer);
            List<String> l = ((DiskPersistWrapper<IPoint>)persistance).ListAttempts(currentPassword,modeNotNull).ToList();
            l.Add("Cancel");
            string session = console.Selection(l, "attempt name:", false);
            if (session == "Cancel")
                return null;
            return ((AttemptSaver)attemptSaver).GetAttempts(currentPassword, session, modeNotNull);
        }

        /// <summary>
        /// Main method to start the program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            MainView v = MainView.Instance();
            Thread t2 = new Thread(() => new Plumber(v));
            t2.IsBackground = true;
            t2.Start();
            Application.Run(v);
        }

    
    }
    enum CurrentMenu { Play, Test1, Test2, Test3, Test4, Test5 };
}
