﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

namespace GazePswd
{
    class DiskPersist : IPersist
    {
        private readonly object lock_obj = new Object();
        ConsoleUi console = ConsoleUi.Instance;
        static readonly Char pathSep = Path.DirectorySeparatorChar;
        static readonly String PWD_DIR = "pwds";
        static readonly String PWD_EXTENSION = ".pwd";
        static readonly String STRENGTH_EXTENSION = ".str";
        static readonly String CULL_EXTENSION = ".cul";
        //hackety crackety
        // {0} is username
        //+{1} password name 
        //+{2} is the attempt or attack name
        static readonly String userDir = PWD_DIR + pathSep + "{0}";
        static readonly String passDir = userDir + pathSep + "{1}";
        static readonly String passFile = passDir + pathSep + "{1}" + PWD_EXTENSION;
        static readonly String strengthFile = passDir + pathSep + "{1}" + STRENGTH_EXTENSION;
        static readonly String attemptsDir = passDir + pathSep + "attempts";
        static readonly String attacksDir = passDir + pathSep + "attacks";
        static readonly String attemptFile = attemptsDir + pathSep + "{2}" + PWD_EXTENSION;
        static readonly String attackFile = attacksDir + pathSep + "{2}" + PWD_EXTENSION;
        static readonly String attemptCullFile = attemptsDir + pathSep + "{2}" + CULL_EXTENSION;
        static readonly String attackCullFile = attacksDir + pathSep + "{2}" + CULL_EXTENSION;
        static readonly String pointRegex = @"\(x=[0-9]+ y=[0-9]+\)";
        static readonly Regex contentLineRegex = new Regex(@"^(" + pointRegex + ", )+" + @"[0-9]+$");



        public DiskPersist()
        {
            //Creates pswd directory if it doesn't exist
            Directory.CreateDirectory(PWD_DIR);
        }

        //
        public IPassword Read(PasswordId id)
        {
            if (!File.Exists(pwdPathOfId(id)))
            {
                return null;
            }
            IPassword pwd = new SimplePassword();
            String path = pwdPathOfId(id);
            IEnumerable<String> file_lines;
            lock (lock_obj)
            {
                file_lines = File.ReadLines(pwdPathOfId(id));
            }
            pwd.ImgPath = file_lines.First(); //first line in a pwd file is the image path
            file_lines = file_lines.Skip(1); //skip over the image path line, we don't need it anymore 
            file_lines = file_lines.Except(new [] {""});
            foreach (String line in file_lines)
            {
                pwd.Add(Point.FromString(line));
            }
            return pwd;
        }

        /// <summary>
        /// Reads attempts from a saved pwd file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="attemptName"></param>
        /// <param name="mode"></param>
        /// <returns>List of attempts for the given id/name combination</returns>
        public IEnumerable<IPassword> ReadAttempt(PasswordId id, String attemptName, LoginMode mode)
        {
            
            String attemptPath = String.Format(mode.Equals(LoginMode.Attempt) ? attemptFile : attackFile, id.UserName, id.PasswordName, attemptName);
         
            List<IPassword> pwds = new System.Collections.Generic.List<IPassword>(); 
            String[] file_lines;
            lock (lock_obj)
            {
                try
                {
                    file_lines = File.ReadLines(attemptPath).ToArray<String>();
                }
                catch (FileNotFoundException)
                {

                    return null;
                }
            }
            for (int i = 0; i < file_lines.Length; i++)
            {
                IPassword pwd = new SimplePassword();
                pwd.ImgPath = file_lines[i++]; //first line in a pwd file is the image path
                while(contentLineRegex.IsMatch(file_lines[i]))
                {
                    pwd.Add(Point.FromString(file_lines[i++]));
                }
                pwds.Add(pwd);
                //After a pwd comes an empty line
            }
            return pwds;
        }

        //Theese functions should be generalised further
        private String pwdPathOfId(PasswordId id)
        {
            return String.Format(passFile, id.UserName, id.PasswordName);
        }

        private String attemptPathOfId(PasswordId id, String name)
        {
            return String.Format(attemptFile, id.UserName, id.PasswordName, name);
        }

        private String attackPathOfId(PasswordId id, String name)
        {
            return String.Format(attackFile, id.UserName, id.PasswordName, name);
        }

        private String attemptCullPathOfId(PasswordId id, String name)
        {
            return String.Format(attemptCullFile, id.UserName, id.PasswordName, name);
        }

        private String attackCullPathOfId(PasswordId id, String name)
        {
            return String.Format(attackCullFile, id.UserName, id.PasswordName, name);
        }

        /// <summary>
        /// Deletes the pswd file for the supplied ID
        /// </summary>
        /// <param name="id"></param>
        public void DeletePswd(PasswordId id)
        {
            if (File.Exists(pwdPathOfId(id)))
            {
                lock (lock_obj)
                {
                    File.Delete(pwdPathOfId(id));
                }
            }
        }

        private PasswordId SafeIdAskUser(PasswordId id)
        {
            PasswordId ret = id;
            int unique = 1;
            lock (lock_obj)
            {
                bool exists = File.Exists(pwdPathOfId(id));
                //if (exists && console.Ask("Username is taken, should I overwrite password? [y/n]: ") == "y")
                //{
                //    File.Delete(pwdPathOfId(id));
                //    return id;
                //}
                while (exists)
                {
                    ret = new PasswordId(id.UserName, id.PasswordName + unique);
                    unique++;
                    exists = File.Exists(pwdPathOfId(ret));
                }
            }
            return ret;
        }

        private String SafeId(PasswordId id, String supplName, Func<PasswordId, String, String> pathOfIdAndName)
        {
            String ret = supplName;
            int unique = 1;
            lock (lock_obj
                )
            {
                bool exists = File.Exists(pathOfIdAndName(id, supplName));
                while (exists)
                {
                    exists = File.Exists(pathOfIdAndName(id, supplName + unique));
                    unique++;
                }
            }
            return ret;
        }

        /// <summary>
        /// writes a paswd to a file
        /// </summary>
        /// <param name="path"></param>
        /// <param name="pwd"></param>
        private void WritePwdToFile(String path, IPassword pwd)
        {
            if (pwd.Points.Count() == 0)
            {
                return;
            }
            IEnumerable<String> pwd_string = pwd.Points.Select(p => p.ToString());
            pwd_string = (new[] { pwd.ImgPath }).Concat(pwd_string); //add image path to beginning of file
            pwd_string = pwd_string.Concat(new[] { "" }); //padding between entries
            lock (lock_obj)
            {
                File.AppendAllLines(path, pwd_string);
            }
        }

        private void setupDirectories(PasswordId id)
        {
            // CreateDirectory creates missing parents as needed (like unix mkdir -p)
            //+so these two commands suffice
            //Also, it has no effect if the dirs already exists, so no checks are needed!
            lock (lock_obj)
            {
                Directory.CreateDirectory(String.Format(attemptsDir, id.UserName, id.PasswordName));
                Directory.CreateDirectory(String.Format(attacksDir, id.UserName, id.PasswordName));
            }
        }

        /// <summary>
        /// Saves password to file. If Id is allready taken, a new id is generated
        /// </summary>
        /// <param name="id">Id of password to save</param>
        /// <param name="pswd">Password to save</param>
        /// <returns>Id of the saved password</returns>
        public PasswordId Write(PasswordId id, IPassword pwd)
        {
            id = SafeIdAskUser(id);
            // ensure dir-structure is in place
            setupDirectories(id);

            // ensure the pwd file exists
            //+and write to it 
            WritePwdToFile(pwdPathOfId(id), pwd);
            return id;
        }

        /// <summary>
        /// Saves attempts or attacks to files
        /// If the attemptName -- id combination allready exists, the attempt is appended to the existing file. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pwd"></param>
        /// <param name="attemptName"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public String WriteAttempt(PasswordId id, IPassword pwd, String attemptName, LoginMode mode)
        {
            Func<PasswordId, String, String> del;
            if (mode.Equals(LoginMode.Attempt)){
                del = attemptPathOfId;
            } else {
                del = attackPathOfId;
            }
            WritePwdToFile(del(id, attemptName), pwd);
            return attemptName;
        }

        /// <summary>
        /// writes strength meassurements to a file, overwrites file if it already exists!
        /// </summary>
        /// <param name="id"></param>
        /// <param name="saliency"></param>
        /// <param name="boundingBox"></param>
        /// <param name="distance"></param>
        public void WriteStrengthData(PasswordId id, String data)
        {
            String path = String.Format(strengthFile, id.UserName, id.PasswordName);
            setupDirectories(id);
            lock (lock_obj)
            {
                File.AppendAllLines(path, new[] { data });
            }
        }

        public void WritePrecullData(PasswordId id, IPassword pwd, String attemptName, LoginMode mode)
        {
            Func<PasswordId, String, String> del;
            if (mode.Equals(LoginMode.Attempt))
            {
                del = attemptCullPathOfId;
            }
            else
            {
                del = attackCullPathOfId;
            }
            WritePwdToFile(del(id, attemptName), pwd); 
        }

        public IEnumerable<string> List()
        {
            //remove directory part from usernames, and return as folder (user) names
            IEnumerable<String> dirs;
            lock (lock_obj)
            {
                dirs = Directory.EnumerateDirectories(PWD_DIR);
            }
            return dirs.Select(
                path => Path.GetFileName(path));
        }

        public IEnumerable<PasswordId> List(String user)
        {
            //If the user folder doesn't exist, return empty enumerable
            lock (lock_obj)
            {
                if (!Directory.Exists(String.Format(userDir, user)))
                {
                    return Enumerable.Empty<PasswordId>();
                }
            }

            IEnumerable<String> dirs;
            lock (lock_obj)
            {
                dirs = Directory.EnumerateDirectories(String.Format(userDir, user));
            }
            return dirs.Select(path => Path.GetFileName(path)).
                Select(pwdName => new PasswordId(user, pwdName));
        }

        public IEnumerable<String> ListAttempts(PasswordId id, LoginMode mode)
        {
            
            String dirPath = String.Format(mode.Equals(LoginMode.Attempt) ? attemptsDir : attacksDir, id.UserName, id.PasswordName);
         
            //If the user folder doesn't exist, return empty enumerable
            lock (lock_obj)
            {
                if (!Directory.Exists(String.Format(dirPath, id.UserName, id.PasswordName)))
                {
                    return Enumerable.Empty<String>();
                }
            }
            
            return Directory.EnumerateFiles(dirPath).Select(
                path => Path.GetFileName(path)).Where(s => s.Contains(".pwd"));
        }

        public bool exists(PasswordId id)
        {
            return List(id.UserName).Any(pwd => pwd.Equals(id));
        }
    }
}