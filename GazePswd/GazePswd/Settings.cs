﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    using System.Drawing;
    using Validation = Tuple<Access, String>;
    using WeightedValidation = Tuple<Tuple<Access, String>, int>;
    class Settings
    {
        //cannot be instantiated
        private Settings() { }

        //Password validation function
        public static Func<OriginalPswd, AttemptPswd, Validation> AnalyzerAggregate = ValidationMethods.relativeTimeBucket;

        //--------------------General settings------------------------------

        //Determine the numbers of quadrants across width in grid
        public static int QuadrantAmountX = 15;
        //Y axis amount not set from get go but calculated according to image aspect ratio
        public static int QuadrantAmountY = 0;

        //Bucket size for pointPogression
        public static int NumberOfPointProgressionBuckets = 5;

        //Amount of buckets in timeBucketingAnalyzer
        public static int NumberOfTimeBuckets = 5;
        
        //use dwell selection
        public static bool UseDwell = false;
        //Use restrictions when recording password
        public static bool useRestrictions = true;

        //turns preemtive culling on and off
        public static bool preCull = true;
        
        //number of allowed sub-attempts per login attempt
        public static int numberOfpswdsPerAttempt = 6;

        

        //---------------Password pre analysis thresholds-------------------
        public static double PreSaliencyThreshold = 0.3;
        public static double PreBoundingBoxThreshold = 0.25;
        public static double PreDistanceThreshold = 0.25;
        public static double PrePointNumberThreshold = 10;
        public static double PrepointNUmberDwellThreshold = 4;

        //Under precull
        public static double LoginSaliencyBound = .5; //50 percent of average

        //------------------------ Login bounds -----------------------------
        //Frechet distance
        public static double frechetAccept = 1.7;
        public static double frechetDeny = 2d;

        //Time bucketed difference in absolute/reel time
        public static double AbsoluteTimeBucketAccept = 200;
        public static double AbsoluteTimeBucketDeny = 500;
        //Time bucketed difference in relative time
        public static double relativeTimeBucketAccept = 0.17;
        public static double relativeTimeBucketDeny = 0.2;
        //Difference in the total time of the passwords compared
        public static double TotalTimeDiffAccept = 0.3;
        public static double TotalTimeDiffDeny = 0.8;
        //Distance bounds for Point Progression analyzer
        public static double PointProgressionAccept = 100;
        public static double PointProgressionDeny = 200; 
        //Distance diff between the two passwords
        public static double distanceComparerAccept = 0.3;
        public static double distanceComparerDeny = 0.8;

        //------------------login weights. 1 to 3. -------------------------
        //INFO: 
        //1 - only one of these are allowed in neutral acceptance
        //2 - two of these are allowed in neutral acceptance
        //3 - take a wild guess buddy :-)
        public static int DiscreteFrechetWeight = 2;
        public static int TotalTimeDiffWeight = 2;
        public static int DistanceComparerWeight = 2;
        public static int AbsoluteTimeBucketWeight = 2;
        public static int PointProgressionWeight = 2;


        //-------------------------------------------------------------------
  
    }
}
