﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GazePswd
{
    /// <summary>
    /// Class that reads and writes to the persistence.
    /// </summary>
    /// <typeparam name="T">Either IPassword or IPoint</typeparam>
    class DiskPersistWrapper<T> : IPlumable<IPoint,IPassword>
    {
       
        //The persistance object.
        private IPersist persistance;

        //Callback delegate called by the pipe.
        private Action<IPoint> del;

        //the pipe to add points to.
        private IPipe<IPassword> writer;

        //THe password to be stored. 
        private IPassword pswd;
       
       
        /// <summary>
        /// Cunstructor
        /// </summary>
        public DiskPersistWrapper()
        {
            persistance = new DiskPersist();
            del = new Action<IPoint>(callback);
            pswd = new SimplePassword();

        }

        public IEnumerable<String> List()
        {
            return persistance.List();
        }

        public IEnumerable<String> ListPswds(String userName)
        {
            return persistance.List(userName).Select(pwd => pwd.ToString());
        }

        public IEnumerable<String> ListAttempts(PasswordId id, LoginMode mode)
        {
            //return persistance.ListAttempts(id, mode).DefaultIfEmpty()
            //    .Aggregate((s1, s2) => s1 + "\n" + s2);
            return persistance.ListAttempts(id, mode).Select((s) => s.Split('.')[0]);
        }

        /// <summary>
        /// Saves the currently stored password points as a password in the persistence. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PasswordId Save(PasswordId id, String imgName)
        {
            
            pswd.ImgPath = imgName;

            verifyPassword();

            //TODO: make it possible to rerecord the password if not satisfied.
            return persistance.Write(id, pswd);
        }

        /// <summary>
        /// Passes the password on the to anlyzer.
        /// </summary>
        private void verifyPassword()
        {
            writer.Write(pswd);
        }

        /// <summary>
        /// Retrieve the password stored for that id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IPassword Restore(PasswordId id)
        {
            return persistance.Read(id);
        }

        public bool exists(PasswordId id)
        {
            return persistance.exists(id);
        }


        /// <summary>
        /// The callback function runs whenever the delegate is invoked and adds
        /// a point to the pswd.
        /// </summary>
        /// <param name="p"></param>
        private void callback(IPoint p)
        {
            IPoint pt = (IPoint) p;
            pswd.Add(pt);
        }

        public void Writer(IPipe<IPassword> connection)
        {
           
            writer = connection;
        }

        public void Reader(IPipe<IPoint> connection)
        {
          
            connection.Subscribe(del);
        }

        /// <summary>
        /// Flushes the contained password, NOT THE PIPES
        /// </summary>
        public void flush()
        {
            pswd = new SimplePassword();
        }
    }
}
