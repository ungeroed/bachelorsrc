﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GazePswd
{
    using Bucket = Dictionary<Tuple<int, int>, double>;

    public class TimeBucketAnalyzer : IAnalyzer
    {

        private string name = "TimeBucketAnalyzer";

        private readonly bool relatime;

        /// <summary>
        /// Initializes TimeBucketAnalyzser
        /// </summary>
        /// <param name="relative_timing">Whether or not to normalize timespans, i.e. consider relative timing rather than absolute timing</param>
        public TimeBucketAnalyzer(bool relative_timing)
        {
            this.relatime = relative_timing;
        }

        /// <summary>
        /// Returns an array of timespans, given a password annotated with timestamps
        /// </summary>
        /// <param name="pswd">A password with timestamps</param>
        private double[] getTimeList(IPassword pswd)
        {
            double[] times = new double[pswd.Points.Count() - 1];
            for (int i = 0; i < times.Length; i++)
            {
                times[i] = (pswd.Points.ElementAt(i + 1).Time - pswd.Points.ElementAt(i).Time).TotalMilliseconds;
            }
            return times;
        }

        private double[] normalize(double[] arr, double normConstant)
        {
            return arr.Select(v => v / normConstant).ToArray();
        }

        /// <summary>
        /// calculates and returns the list of point per bucket in timeslots
        /// </summary>
        /// <returns></returns>
        private Bucket[] getBucketList(IPassword pswd, double[] times)
        {
            //The total startTime on last point are not recorded.
            double bucketSize = getBucketSize(times);

            //Each dictionary is a histogram of point coords
            //List<Bucket> bucketList = new List<Bucket>();
            Bucket[] bucketArr = new Bucket[(int) Settings.NumberOfTimeBuckets];

         
            double individualBucketCounter = bucketSize; 
            Tuple<int,int> currentCoords;
            int currentTimePoint = 0;
            
            
            //fill with bucket dictionaries
            for (int i = 0; i < bucketArr.Length; i++)
            {
                Bucket newBucket = new Bucket();
                individualBucketCounter = bucketSize; 
                //Create the bucket dictionary
                while (individualBucketCounter > 0 && currentTimePoint != times.Length)
                {
                    currentCoords = pswd.Points.ElementAt(currentTimePoint).Coords.ToTuple();
                    //room for it all
                    if (individualBucketCounter > times[currentTimePoint])
                    {
                        if (newBucket.ContainsKey(currentCoords))
                            newBucket[currentCoords] += times[currentTimePoint];
                        else
                        {
                            newBucket.Add(currentCoords, times[currentTimePoint]);
                        }
                        
                    }
                    //else fill bucket to the rim and break
                    else
                    {
                        if (newBucket.ContainsKey(currentCoords))
                            newBucket[currentCoords] += individualBucketCounter;
                        else
                        {
                            newBucket.Add(currentCoords, individualBucketCounter);
                        }
                        //the rest of this will be added in next bucket after we break
                        times[currentTimePoint] -= individualBucketCounter;
                        break;
                    }
                    //decrement bucketCounter
                    individualBucketCounter -= times[currentTimePoint]; 
                    currentTimePoint++;
                }
                

                bucketArr[i] = newBucket;
            }
            return bucketArr;   
        }

        /// <summary>
        /// calculates bucketsize
        /// </summary>
        /// <param name="pswd"></param>
        /// <returns></returns>
        private double getBucketSize(double[] times)
        {
            return times.Sum() / Settings.NumberOfTimeBuckets;
        }

        public string Name
        {
            get { return name; }
        }

        public double Match(OriginalPswd orig, AttemptPswd attempt)
        {
            double[] origTimeList = getTimeList(orig);
            double[] attemptTimeList = getTimeList(attempt);
            if (relatime)
            {
                double normConstant = origTimeList.Sum();
                origTimeList = normalize(origTimeList, normConstant);
                attemptTimeList = normalize(attemptTimeList, attemptTimeList.Sum());
            }


            Bucket[] origBucketList = getBucketList(orig, origTimeList);
            Bucket[] attemptBucketList = getBucketList(attempt,attemptTimeList);

            Debug.Assert(attemptBucketList.Count() == origBucketList.Count() && attemptBucketList.Count() == Settings.NumberOfTimeBuckets);


            double[] resultArr = new double[(int) Settings.NumberOfTimeBuckets];

            for (int i = 0; i < resultArr.Length; i++)
            {
                //arrrr matey ye best be waiting a bit for this
                resultArr[i] = compareBuckets(origBucketList[i], attemptBucketList[i]);
            }

            double totalTime = (orig.Points.Last().Time - orig.Points.First().Time).TotalMilliseconds;

            if (relatime)
            {
                return resultArr.Sum();
            }
            else return resultArr.Sum() / totalTime;
           
        }

        private double compareBuckets(Bucket orig, Bucket attempt)
        {
            KeyValuePair<Tuple<int, int>, double> b2Order = orig.OrderByDescending(s => s.Value).First();
            Coords cord1 = new Coords(b2Order.Key.Item1, b2Order.Key.Item2);
            IEnumerable<KeyValuePair<Tuple<int, int>, double>> b1Order = attempt.OrderByDescending(s => s.Value).Take(3)
                .OrderBy(point => cord1.DistanceTo(new Coords(point.Key.Item1, point.Key.Item2)));

            
            //increase hits on highest counted point to minimize spread
            double first = b2Order.Value + ((orig.Count() > 1) ? orig.OrderByDescending(s => s.Value).ElementAt(1).Value : 0);
            double second = b1Order.First().Value + ((b1Order.Count() > 1) ? b1Order.ElementAt(1).Value : 0);
            double ret = Math.Abs(first - second);
            if (!relatime)
            {
                ret = Math.Pow(ret, 2);
            }
            return ret;
        }

        
    }
}