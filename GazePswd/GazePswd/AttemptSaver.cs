﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GazePswd
{
    /// <summary>
    /// Class that reads and writes attempts to the persistence.
    /// </summary>
    /// <typeparam name="T">Either IPassword or IPoint</typeparam>
    class AttemptSaver : IPlumable<IPassword, IPassword>
    {
        //The persistance object.
        private IPersist persistance;

        //Callback delegate called by the pipe.
        private Action<IPassword> del;

        //the pipe to add points to.
        private IPipe<IPassword> writer;

        //name of attempt
        String attemptName;

        //Enable or disable saving of attempts
        private bool saveEnabled = false;

        //user ID of the current user
        private PasswordId UserId;
        private LoginMode loginMode;

        /// <summary>
        /// Cunstructor
        /// </summary>
        public AttemptSaver()
        {
            persistance = new DiskPersist();
            del = new Action<IPassword>(callback);     
        }

      

        /// <summary>
        /// Enables saving of all password attempts
        /// </summary>
        /// <param name="userId"></param>
        public void EnableSaveAttempts(PasswordId userId, string attemptName, LoginMode mode)
        {
            saveEnabled = true;
            this.UserId = userId;
            this.attemptName = attemptName;
            this.loginMode = mode;

        }

        /// <summary>
        /// Disabling saving of password attempts
        /// </summary>
        public void DisableSaveAttempts()
        {
            saveEnabled = false;
        }

        /// <summary>
        /// Returns a list of all attempts on that specific user ID.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<IPassword> GetAttempts(PasswordId userId, String attemptName, LoginMode mode)
        {
            return persistance.ReadAttempt(userId, attemptName, mode);
        }

        /// <summary>
        /// The callback function runs whenever the delegate is invoked and adds
        /// a point to the pswd.
        /// </summary>
        /// <param name="p"></param>
        private void callback(IPassword pwd)
        {
            if (writer != null)
            {
                writer.Write(pwd);
            }
            if (saveEnabled)
            {
                persistance.WriteAttempt(UserId, pwd, attemptName, loginMode);
            }
          
        }

        public void Writer(IPipe<IPassword> connection)
        {       
            writer = connection;
        }

        public void Reader(IPipe<IPassword> connection)
        {           
            connection.Subscribe(del);
        }
    }
}
