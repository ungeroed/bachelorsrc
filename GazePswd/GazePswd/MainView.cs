﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Tobii.EyeX.Client;
using Tobii.EyeX.Framework;

namespace GazePswd
{
    public partial class MainView : Form
    {
        //list of image paths
        string[] imageFiles;

        //A list of paths for saliency images
        string[] saliencyFiles;

        //the current image pointer
        private int currentImgPtr = int.MaxValue;

        //Path of the current image
        public string CurrentImg;

        //draw gazepoints live
        bool enableGazePoints = false;

        //Decides wether you can switch image
        private bool fixedImage = false;

        //Current Backgroundimage
        public Bitmap CurrentPswdImage = null;

        //True if saliency image is currently the shown image
        private bool saliencyIsCurrentlyShown = false;

        //The fallback Mousetracker
        MouseTracker mouseTracker;
        private static MainView instance;

        public static MainView Instance()
        {
            //Shorthands - bitch!
            //No, but it's called a null coalescing opperater, look it up.
            instance = instance ?? new MainView();
            return instance;
        }
        
        private MainView()
        {
            InitializeComponent();
            imageFiles = Directory.GetFiles(@"imgs/");
            saliencyFiles = Directory.GetFiles(@"saliencyMix/");
            //Bitmap b = new Bitmap(imageFiles[currentImgPtr]);
            Bitmap logoImg = new Bitmap("Logo.jpg");
            this.BackgroundImage = logoImg;
            //CurrentImg = imageFiles[currentImgPtr];
            CurrentPswdImage = logoImg;
            


            this.FormBorderStyle = FormBorderStyle.None;
            this.BackgroundImageLayout = ImageLayout.None;
            this.WindowState = FormWindowState.Maximized;


        }

        public void ShowLogo()
        {
            BackgroundImage = new Bitmap("Logo.jpg");
        }

        /// <summary>
        /// Replaces the current image with the saliency map
        /// </summary>
        public void ShowSaliency()
        {
            BackgroundImage = new Bitmap(saliencyFiles[currentImgPtr]);
            saliencyIsCurrentlyShown = true;
        }

        /// <summary>
        /// Cycles to the next image
        /// </summary>
        public void nextImage()
        {
            if (fixedImage)
                return;
            if (currentImgPtr >= imageFiles.Length-1)
            {
                currentImgPtr = 0;
            } 
            else {
                currentImgPtr++;
            }
            Bitmap b = new Bitmap(imageFiles[currentImgPtr]);
            this.BackgroundImage = b;
            CurrentImg = imageFiles[currentImgPtr];
            CurrentPswdImage = b;
        }

        public void DrawGaze(System.Drawing.Point p)
        {
            if (!enableGazePoints)
            {
                return;
            }
            if (InvokeRequired)
            {
                Invoke(new Func<System.Drawing.Point, int>(drawOnInvoke), new Object[] { p });
            }
            else 
            {
                drawOnInvoke(p);
            }
           
           
        }

        /// <summary>
        /// Enables of disables the switching of pswd image.
        /// </summary>
        /// <param name="b"></param>
        public void FreezeImage(bool b)
        {
            fixedImage = b;
        }


        /// <summary>
        /// toggles the live drawing of gaze points
        /// </summary>
        public void ToggleGaze()
        {
          
            enableGazePoints = !enableGazePoints;
        }     

        private int drawOnInvoke(System.Drawing.Point p)
        {
            System.Drawing.Graphics formGraphics = this.CreateGraphics();
            formGraphics.FillEllipse(Brushes.Red,p.X,p.Y,10,10);
            return 0;
        }

        /// <summary>
        /// draws the quadrant according to quadrant size
        /// </summary>
        /// <param name="pswd"></param>
        public void DrawQuadrants(IPassword pswd)
        {

            double squareX = CurrentPswdImage.Width / Settings.QuadrantAmountX;
            int YAmount = (int)(CurrentPswdImage.Height / squareX);
            double squareY = CurrentPswdImage.Height / YAmount;
            
            foreach (IPoint origin in pswd.Points)
            {                        
                System.Drawing.Graphics graphics = this.CreateGraphics();
                System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(
                   (int)(origin.Coords.X * squareX), (int)(origin.Coords.Y * squareY), (int)squareX, (int) squareY);
                
                graphics.DrawRectangle(System.Drawing.Pens.Yellow, rectangle);
            }
           

        }

        /// <summary>
        /// sets the current image based on the image that was used to record the given pswd.
        /// </summary>
        /// <param name="p"></param>
        public void SetPswdImg(IPassword p)
        {
            Bitmap b = new Bitmap(p.ImgPath);
            this.BackgroundImage = b;
            CurrentPswdImage = b;
            //setting the correct image pointer
            for (int i = 0; i < imageFiles.Length; i++)
            {
                if (imageFiles[i].Equals(p.ImgPath))
                {
                    currentImgPtr = i;
                    break;
                    
                }

            }
            CurrentImg = p.ImgPath;

          
        }


        /// <summary>
        /// Writes the current txt as a message to the user on the screen
        /// </summary>
        /// <param name="txt"></param>
        public void DrawText(string txt, int color) //A number for a color? #wat
        {
            System.Drawing.Graphics formGraphics = this.CreateGraphics();
            
            System.Drawing.Font drawFont = new System.Drawing.Font("Arial", 20);
            
            Color c = Color.Snow;
            switch (color)
            {
                case 1:
                    c = Color.Green;
                    break;
                case 2:
                    c = Color.Red;
                    break;
            }

            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(c);
            int x = 100;
            int y = 100;
            System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
            formGraphics.DrawString(txt, drawFont, drawBrush, x, y, drawFormat);
            drawFont.Dispose();
            drawBrush.Dispose();
            formGraphics.Dispose();
        }
        /// <summary>  
        /// Draws the grid on the image according to quadrantSize
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public void drawGrid()
        {

            double squareX = CurrentPswdImage.Width / Settings.QuadrantAmountX;
            int YAmount = (int)(CurrentPswdImage.Height / squareX);
            double squareY = CurrentPswdImage.Height / YAmount;

            
            //double relation = BackgroundImage.Width / BackgroundImage.Height;
            //int squareY = (int)(BackgroundImage.Height / Settings.QuadrantAmountX);
            for (int i = 1; i < Settings.QuadrantAmountX; i++)
            {
              
                System.Drawing.Graphics formGraphics = this.CreateGraphics();
                formGraphics.DrawLine(Pens.Blue, (int)(i * squareX), 0, (int)(i * squareX), CurrentPswdImage.Height);             
                formGraphics.Dispose();

            }

            for (int i = 1; i < YAmount; i++)
            {
                System.Drawing.Graphics formGraphics = this.CreateGraphics();
                formGraphics.DrawLine(Pens.Blue, 0, (int)(i * squareY), CurrentPswdImage.Width, (int)(i * squareY));
            }
          
        }

        /// <summary>
        /// draws a box on the image
        /// </summary>
        /// <param name="rect"></param>
        public void DrawBox(Rect rect)
        {
            System.Drawing.Graphics graphics = this.CreateGraphics();
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(
               rect.X, rect.Y, rect.Width, rect.Height);

            graphics.DrawRectangle(System.Drawing.Pens.Yellow, rectangle);

        }

        /// <summary>
        /// Clears the show image
        /// </summary>
        public void ClearImage()
        {
            if(saliencyIsCurrentlyShown)
            {
                BackgroundImage = CurrentPswdImage;
                saliencyIsCurrentlyShown = false;
            }

            Invalidate();
           
        }

        /// <summary>
        /// Draws a password
        /// </summary>
        /// <param name="pswd"></param>
        /// <param name="color"></param>
        public void DrawPassword(IPassword pswd, int color)
        {
            if (pswd == null)
            {
                return;
            }
            Point lastPoint = new Point();
            Boolean set = false;
            foreach (Point p in pswd.Points)
            {
                if (!set)
                {
                    lastPoint = p;
                    set = true;
                }
                else
                {
                    drawPath(lastPoint, p, color);
                    lastPoint = p;
                }
            }
        }


        /// <summary>
        /// Draws a path from a to b.
        /// </summary>
        /// <param name="a">Point</param>
        /// <param name="b">Point</param>
        /// <param name="color">only values 1 or 2 are valid colors atm.</param>
        private void drawPath(IPoint a, IPoint b, int color)
        {
            System.Drawing.Graphics formGraphics = this.CreateGraphics();
            Pen p = new Pen(Brushes.Red, 2.0F);
            
            //we only want to draw from center of each quadrant. 
            double squareX = CurrentPswdImage.Width / Settings.QuadrantAmountX;
            int YAmount = (int)(CurrentPswdImage.Height / squareX);
            double squareY = CurrentPswdImage.Height / YAmount;



            switch (color)
            {
                case 1:
                    formGraphics.DrawLine(p, (int)(a.Coords.X*squareX+0.5*squareX), (int)(a.Coords.Y*squareY+0.5*squareY), (int)(b.Coords.X*squareX+0.5*squareX), (int)(b.Coords.Y*squareY+0.5*squareY));
                    break;
                case 2:
                    p.Color = Color.Black;
                    formGraphics.DrawLine(new Pen(Brushes.Orange, 3.0F), a.PixelCoords.X, a.PixelCoords.Y, b.PixelCoords.X, b.PixelCoords.Y);
                    break;
               
            }
            formGraphics.Dispose();
        }

        /// <summary>
        /// Enables the fallback mousetracker
        /// </summary>
        public void EnableMouseTracker()
        {
            mouseTracker = MouseTracker.Instance;
            this.MouseMove += mouseTracker.CaptureMouse;
        }


       
    }
}
