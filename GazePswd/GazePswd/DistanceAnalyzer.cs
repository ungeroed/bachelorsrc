﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class DistanceAnalyzer : IAnalyzer
    {

        public double Match(OriginalPswd orig, AttemptPswd attempt)
        {
            double dist1 = calculateDistance(orig); 
            return Math.Abs(dist1 - calculateDistance(attempt))/dist1;
        }

        public String Name
        {
            get { return "Distance"; }
        }

        public double GetDistancePercent(IPassword pass)
        {
            if (pass.Points.Count() == 0)
                return 0.01;

            double d = calculateDistance(pass);
            double imageCircumference = pass.Points.First().CoordAmount.X * 2 + pass.Points.First().CoordAmount.Y * 2;
            return d / imageCircumference;
        }

        /// <summary>
        /// Calculates the distance of a password
        /// </summary>
        /// <param name="pass"></param>
        /// <returns></returns>
        public double calculateDistance(IPassword pass)
        {
            if (pass.Points.Count() == 0)
                return 1.00;

            IPoint comparePoint = pass.Points.First();
            double distance = 0.0;
            foreach (IPoint p in pass.Points)
            {   
                distance += comparePoint.Coords.DistanceTo(p.Coords);
                comparePoint = p;
            }
            return distance;
        }
    }
}
