﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GazePswd
{
    class Validator : IPlumable<IPassword,string>
    {
        //pswd candidates
        //This queue is threadsafe.
        private BlockingCollection<IPassword> queue; 

        //Callback delegate called by the pipe.
        private Action<IPassword> del;

        //the pipe to add points to.
        private IPipe<string> writer;

        //The password we're comparing with
        private static OriginalPswd pass;

        private AttemptStopper attemptStopper;

        /// <summary>
        /// Analyzes a password and determines weather it matches.
        /// </summary>
        public Validator()
        {
            queue = new BlockingCollection<IPassword>(new ConcurrentQueue<IPassword>());
            del = new Action<IPassword>(process);
            
            //Starting pool of four threads for analyzation
            ThreadPool.SetMaxThreads(8, 4);
            attemptStopper = AttemptStopper.Instance;
   
        }

        /// <summary>
        /// Sets the current pswd
        /// </summary>
        /// <param name="pswd"></param>
        public static void SetPswd(IPassword pswd)
        {
            pass = (OriginalPswd) pswd;
        }

        /// <summary>
        /// This method is used by the threadpool to analyze each pswd candidate
        /// </summary>
        /// <param name="threadContext"></param>
        private void processQueue(Object threadContext)
        {
            AttemptPswd pswd = (AttemptPswd) queue.Take(); //this blocks if there are no items in the queue.
            Tuple<Access, String> res = Settings.AnalyzerAggregate.Invoke(pass, pswd);
            String results = "";

            switch (res.Item1)
            {
                case Access.Granted:
                    results = "Access granted\n";
                    attemptStopper.handler.Invoke(res.Item1);
                    break;
                case Access.Neutral:
                    results = "Access neutral\n";
                    attemptStopper.Analyze(pass, pswd);
                    break;
                case Access.Denied:
                    results = "Access denied\n";
                    attemptStopper.Analyze(pass, pswd);
                    break;
                default:
                    throw new InvalidOperationException("That's not an accepted Access signifier");
            }
            writer.Write(results + res.Item2);
        }

        /// <summary>
        /// Method called by the delegate
        /// </summary>
        /// <param name="pswd"></param>
        private void process(IPassword pswd)
        {
            queue.Add(pswd);
            //Adds a job to the thread queue. The job will execute when a thread is available
            ThreadPool.QueueUserWorkItem(processQueue, 1);
        }


        public void Reader(IPipe<IPassword> connection)
        {
            connection.Subscribe(del);
        }


        public void Writer(IPipe<string> connection)
        {
            writer = connection;
        }

    }
    enum Access { Granted, Neutral, Denied, SaliencyDenied };
}
