﻿using System;
using System.Collections.Generic;

namespace GazePswd
{
    public interface IPassword
    {
        IEnumerable<IPoint> Points
        {
            get; 
        }

        void Add(IPoint point);

        String ImgPath
        { get; set; }

        IPassword Clone();
    }

    public interface OriginalPswd : IPassword { };

    public interface AttemptPswd : IPassword { };
}
