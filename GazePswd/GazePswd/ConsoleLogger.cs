﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class ConsoleLogger :IPlumable<string,string>
    {
        private Action<string> del;

        /// <summary>
        /// public constructor for your pleasure...
        /// </summary>
        public ConsoleLogger()
        {
            del = new Action<string>(callback);
        }

        /// <summary>
        /// callback method run by the delegate
        /// writes out all received strings.
        /// </summary>
        /// <param name="s"></param>
        private void callback(String s)
        {
            Console.Out.WriteLine(s);
        }

        public void Reader(IPipe<string> connection)
        {
            connection.Subscribe(del);
        }

        public void Writer(IPipe<string> connection)
        {
            throw new NotImplementedException();
        }
    }
}
