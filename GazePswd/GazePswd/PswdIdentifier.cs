﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace GazePswd
{
    class PswdIdentifier : IPlumable<IPoint,IPassword>
    {

        IPassword prePwd = new SimplePassword();
        
        //Delegate to handle incomming IPoints
        private Action<IPoint> del;

        //Pipe to write to
        private IPipe<IPassword> writer;

        //list that holds all pswd contestands
        private List<IPassword> pswdList;

        //The distance that a point must be from the starting and ending points to start
        //and finish possible pswds.
        private const int ThresHold = 2; 

        //the current loaded password to compare with
        private IPassword pass;

        private PasswordId pwdId;

        private LoginMode mode;

        private String attemptName ;

      

        private IPersist persistance = new DiskPersist();

        private static SaliencyAnalyzer salAnalyzer;

        /// <summary>
        /// Constructor
        /// </summary>
        public PswdIdentifier()
        {
            pswdList = new List<IPassword>();
            del = new Action<IPoint>(analyze);
        }


        /// <summary>
        /// Sets the current pswd, and initializes the saliencyAnalyzer
        /// </summary>
        /// <param name="pswd"></param>
        public void SetPswd(PasswordId id, IPassword pswd, String attemptName, LoginMode attemptMode)
        {
            this.pwdId = id;
            this.pass = pswd;
            this.attemptName = attemptName;
            this.mode = attemptMode;
            salAnalyzer = SaliencyAnalyzer.Instance;
        }

        /// <summary>
        /// Resets the gathered points to zero, and restarts pre culling.
        /// </summary>
        public void flush()
        {
            pswdList.RemoveAll(s => true);
            Settings.preCull = true;
            prePwd = new SimplePassword();

        }

        /// <summary>
        /// Callback method used by the delegate.
        /// NOTE:This could be changed to use euclidian distance instead
        /// </summary>
        /// <param name="p"></param>
        private void analyze(IPoint p)
        {
           

            //then add the point to all pswd candidates.
            foreach (IPassword psw in pswdList)
                psw.Add(p);

            if (pass == null)
            {
                Console.Out.WriteLine("Stored password not loaded...");
                return;
            }
            //if the starting quadrant is hit
            IPoint firstP = pass.Points.ElementAt(0);
            if (p.Coords.Equals(firstP.Coords))
            {
                Console.Out.WriteLine("First quadrant hit. new password started");
                IPassword ps = new SimplePassword();
                ps.ImgPath = pass.ImgPath;
                ps.Add(p);
                pswdList.Add(ps);
                Settings.preCull = false;
                prePwd = null; // make pre-password avalable for garbage collecting

            }

            //as long as the starting point hasnt been hit, we preculling users
            if (Settings.preCull)
            {
                prePwd.Add(p);
                save(prePwd);
                PreCull(prePwd);
            }


            //if the ending quadrant is hit within threshold - ship all pswds off for further analysis.
            IPoint lastP = pass.Points.Last();
            if (p.Coords.Equals(lastP.Coords))
            {
                //We flush every time we ship of a new load of attempts.
                AttemptStopper.Instance.Flush();
                foreach (IPassword psw in pswdList)
                {
                    if (Settings.UseDwell || psw.Points.Count() > 9)
                    {
                        writer.Write(psw.Clone());
                    }
                }
            }

        }

        private void save(IPassword prePwd)
        {
            persistance.WritePrecullData(pwdId, prePwd, attemptName, mode);
        }

        /// <summary>
        /// This method handles  preculling based on saliency
        /// </summary>
        /// <param name="p"></param>
        private void PreCull(IPassword p)
        {
            //This 10 point minimum culling data ensures that you dont get thrown out based on very first point.
            if (p.Points.Count() < 10)
            {
                return;
            }
            if (Settings.useRestrictions && !salAnalyzer.VerifyPassword(p))
            {
                Console.Out.WriteLine("Saliency threshold reached... This is the first time you're viewing this image! Access denied!");
                AttemptStopper.Instance.handler.Invoke(Access.SaliencyDenied);
            }
        }

       

        public void Reader(IPipe<IPoint> connection)
        {
            IPipe<IPoint> con = (IPipe<IPoint>)connection;
            con.Subscribe(del);
        }

        public void Writer(IPipe<IPassword> connection)
        {
            writer = connection;
        }
    }
}
