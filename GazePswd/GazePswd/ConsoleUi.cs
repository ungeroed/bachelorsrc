﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class ConsoleUi
    {
        private static ConsoleUi instance;
        
        private IPipe<Question> questionQueue;

        private readonly object lock_obj = new Object();

        private ConsoleUi()
        {
            questionQueue = new AsyncPipe<Question>();
            questionQueue.Subscribe(question =>
                question.del.Invoke(Ask(question.question)));
        }

        public static ConsoleUi Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConsoleUi();
                }
                return instance;
            }
        }
        
        public void Tell(String tale)
        {
            Console.WriteLine(tale);
        }

        public string Ask (String question){
            lock(lock_obj) {
                Console.WriteLine(question);
                return Console.ReadLine();
            }
        }

        public void Ask(String question, Action<String> del)
        {
            questionQueue.Write(new Question() { del = del, question = question });
        }

        private class Question
        {
            public Action<String> del;
            public String question;
        }

        /// <summary>
        /// This method makes it easy to select the coices in the array.
        /// The coice is selected with the arrow keys and confirmed with the return key
        /// </summary>
        /// <param name="strArr"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public string Selection(List<string> stringArr, string name, bool includeNew)
        {
            string[] strArr = stringArr.ToArray();
            //If it should be possible to add a new item, we add that selection at the last position
            if (includeNew)
            {
                stringArr.Add("New");
                strArr = stringArr.ToArray();
            }

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("Please select {0}:", name);
            Console.ResetColor();

            for (int i = 0; i < strArr.Length; i++)
            {
                Console.Out.WriteLine(strArr[i]);
            }

            string returnString = "";
            int index = strArr.Length;

            //highlight the bottom one at start
            Console.SetCursorPosition(0, index);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(strArr[index - 1]);

            var k = Console.ReadKey();
            while (true)
            {

                if (k.Key == ConsoleKey.UpArrow)
                {
                    //draw previous choice black again
                    Console.SetCursorPosition(0, index);
                    Console.ResetColor();
                    Console.Write(strArr[index - 1]);

                    if (index == 1)
                    {
                        index = strArr.Length;
                    }
                    else
                    {
                        index--;
                    }

                    //Highlight selected item
                    Console.SetCursorPosition(0, index);
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(strArr[index - 1]);
                    Console.ResetColor();
                }

                if (k.Key == ConsoleKey.DownArrow)
                {
                    //draw previous choice black again
                    Console.SetCursorPosition(0, index);
                    Console.ResetColor();
                    Console.Write(strArr[index - 1]);

                    if (index == strArr.Length)
                    {
                        index = 1;
                    }
                    else
                    {
                        index++;
                    }

                    //Highlight selected item
                    Console.SetCursorPosition(0, index);
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(strArr[index - 1]);
                    Console.ResetColor();
                }

                if (k.Key == ConsoleKey.Enter)
                {
                    Console.ResetColor();
                    Console.Clear();
                    //Console.Out.WriteLine(strArr[index - 1]);
                    returnString = strArr[index - 1];
                    break;
                }
                 if (k.Key == ConsoleKey.Spacebar)
                {
                    Console.ResetColor();
                     Console.Clear();
                     return Selection(stringArr, name, includeNew);
                }

                k = Console.ReadKey();
            }
            return returnString;
        }
    }
}