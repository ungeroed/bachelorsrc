﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GazePswd
{
    public class DiscreteFrechetAnalyser : IAnalyzer
    {
        public String Name
        {
            get { return "Discrete Fretchet Distance"; }
        }

        public double Match(OriginalPswd orig, AttemptPswd attempt)
        {
            int n = attempt.Points.Count();
            int m = orig.Points.Count();
            //name adopted from [EM94]
            double[,] ca = new double[n, m];

            //probably takes linear startTime, can be optimized if we _know_ the implementation of Points supports constant startTime IEnumerable.ElementAt(i)
            IPoint[] P = attempt.Points.ToArray();
            IPoint[] Q = orig.Points.ToArray();

            //initialize shortest to negative value
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    ca[i, j] = -1d;
                }
            }
            return C(n-1, m-1, ca, P, Q);
        }

        private static double Distance(IPoint p, IPoint q) {
            return p.Coords.DistanceTo(q.Coords);
        }

        //names C and ca adopted from [EM94]
        private static double C(int i , int j, double[,] ca, IPoint[] P, IPoint[] Q)
        {
            if (ca[i, j] > -1) return ca[i, j];
            else if (i == 0 && j == 0) ca[i, j] = Distance(P[0], Q[0]);
            else if (i > 0 && j == 0) ca[i, j] = Math.Max(C(i - 1, 0, ca, P, Q), Distance(P[i], Q[0]));
            else if (i == 0 && j > 0) ca[i, j] = Math.Max(C(0, j - 1, ca, P, Q), Distance(P[0], Q[j]));
            else if (i > 0 && j > 0)
            {
                double[] arr = { C(i - 1, j, ca, P, Q), C(i - 1, j - 1, ca, P, Q), C(i, j - 1, ca, P, Q) };
                ca[i, j] = Math.Max(arr.Min(), Distance(P[i], Q[j]));
            }
            else ca[i, j] = Double.PositiveInfinity;
            return ca[i, j];
        }
    }
}
