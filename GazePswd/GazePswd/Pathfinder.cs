﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace GazePswd
{
    class Pathfinder : IPlumable<IPoint,IPoint>
    {
        
        private IPoint lastStored = null;

        private Action<IPoint> del;

        private IPipe<IPoint> writer;

        private LinkedList<IPoint> dwellList;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="quadrantAmount">numbers of tiles in the grid</param>
        /// <param name="xSide">image x axis</param>
        /// <param name="ySide">image y axis</param>
        public Pathfinder()
        {
            del = new Action<IPoint>(myCallback);
            dwellList = new LinkedList<IPoint>(); 
        }




        //this could be refactored to analytic class..
        private void Analyze(IPoint point)
        {
            IPoint pnt = GetQuadrant(point);
            pnt.CoordAmount.X = Settings.QuadrantAmountX;
            
            //This could be placed inside the getquadrant method to not accept multible of the same points
                if (Settings.UseDwell)
                {
                    dwell(point);
                } 
                else
                {
                    StoreQuadrant(pnt);
                }
        }
        
        

        /// <summary>
        /// Returns the quadrant origin that belongs to the current tracked point.
        /// </summary>
        /// <param nThe point of origin for the belonging squareame="point"> The tracked gaze point</param>
        /// <returns></returns>
        private IPoint GetQuadrant(IPoint point)
        {
            double squareX = point.PixelBound.X / Settings.QuadrantAmountX;
            int YAmount = (int)(point.PixelBound.Y / squareX);
            double squareY = point.PixelBound.Y / YAmount;
            if (Settings.QuadrantAmountY == 0)
                Settings.QuadrantAmountY = YAmount;
            int x = (int) (point.PixelCoords.X / squareX);  
            int y = (int) (point.PixelCoords.Y / squareY);
            
            //This is to counter the case where the outermost pixel is selected
            x = (x == Settings.QuadrantAmountX) ? x - 1 : x;
            y = (y == Settings.QuadrantAmountY) ? y - 1 : y;

            point.Coords = new Coords((int)x, (int)y);
            point.CoordAmount = new Coords();
            point.CoordAmount.Y = Settings.QuadrantAmountY;
            return point;
        }



        /// <summary>
        /// stores the square if its a new square. 
        /// </summary>
        /// <param name="point">The point</param>
        private void StoreQuadrant(IPoint point)
        {   
            
            //If its the first quadrant origin to be stored just store it.
            if (lastStored == null)
            {
                writer.Write(point);
                lastStored = point;
                lastStored.Time = DateTime.Now;
                return;
            } else
            //If there are elements in the list already, check that we're not adding duplets.            
            if (!point.Coords.Equals(lastStored.Coords))
            {
                if (writer != null)
                {
                    writer.Write(point);
                    lastStored = point;
                    lastStored.Time = DateTime.Now;
                }
                

            }
            return;
        }

        /// <summary>
        /// If dwell startTime is enabled, this method will handle the check for dwelling.
        /// </summary>
        /// <param name="point"></param>
        private void dwell(IPoint point)
        {
            //if the dwell list is emtpy, just at the point and get on with it
            if (dwellList.Count == 0)
            {
                dwellList.AddFirst(point);
                return;
            }

            //Else we get the first point to compare with
            IPoint firstPoint = dwellList.First();
 
            //if the point is not the bounds of the first in the list/que dont return
            if (withinPerimiter(firstPoint, point))
            {
               
                if (dwellTimeLimitReached(firstPoint, point))
                {
                    
                    dwellList.AddLast(point);
                    IPoint averagePoint = getAverageAndEmtpyQueue();
                    writer.Write(averagePoint);
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer("ding.wav");
                    player.Play();
                    dwellList = new LinkedList<IPoint>();
                    return;

                }
                else
                {
                    //In this case, the point is still within bounds, but the dwelling has not been kept long enough.
                    //so we just add the point to the list and returns.
                    dwellList.AddLast(point);
                    return;
                }
            }
            else
            {
                //If its not within the parameter of the first point, we start a new queue.
                dwellList = new LinkedList<IPoint>();
                dwellList.AddLast(point);
            }
           
        }

        /// <summary>
        /// returns true if the second point are within the same 3x3 quadrant
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private bool withinPerimiter(IPoint a, IPoint b)
        {
            bool within = true;
            within = (Math.Abs(a.Coords.X - b.Coords.X) < 3);
            within = (Math.Abs(a.Coords.Y - b.Coords.Y) < 3);
            return within;
        }

        /// <summary>
        /// Returns true if the dwell startTime limit has been reached from the first point to the last
        /// </summary>
        /// <returns></returns>
        private bool dwellTimeLimitReached(IPoint a, IPoint b)
        {
            double time = (b.Time - a.Time).TotalMilliseconds;
            return (time > 3000);   
        }

        private IPoint getAverageAndEmtpyQueue()
        {
            Dictionary<Tuple<int,int>,int> dict = new Dictionary<Tuple<int,int>,int>();
            foreach(IPoint p in dwellList)
            {
                Tuple<int,int> t = new Tuple<int,int>(p.Coords.X,p.Coords.Y);
                if (dict.ContainsKey(t))
                {
                    int nr = dict[t];
                    nr++;
                    dict[t]++;
                    
                }
                else
                {
                    dict.Add(t, 1);
                }
            }
            //If several quads has the same visited startTime.
            IEnumerable<KeyValuePair<Tuple<int,int>,int>> en = dict.Where((s) => s.Value == dict.Max((st) => st.Value));

            //just grab the coords from the first and assign to the first point from the list.
            IPoint pt = dwellList.First();
           
            Coords cds = new Coords();
            cds.X = en.ElementAt(0).Key.Item1;
            cds.Y = en.ElementAt(0).Key.Item2;
            pt.Coords = cds;
            return pt;


        }

        /// <summary>
        /// Callback method used by the delegate
        /// </summary>
        /// <param name="p"></param>
        private void myCallback(IPoint p){
            Analyze(p);
        }

        public void Writer(IPipe<IPoint> connection)
        {
            writer = connection;
        }

        public void Reader(IPipe<IPoint> connection)
        {
            connection.Subscribe(del);
        }
    }
}
