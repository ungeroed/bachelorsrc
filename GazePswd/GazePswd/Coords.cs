﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    public class Coords
    {
        
        public int X;
        public int Y;

        public Coords(int x, int y)
        {
            this.X = x; this.Y = y;
        }

        public Coords() { }

        public Double DistanceTo(Coords comparePoint)
        {
            return Math.Sqrt(Math.Pow((comparePoint.X - this.X), 2) + Math.Pow((comparePoint.Y - this.Y), 2));
        }

        public Tuple<int, int> ToTuple()
        {
            return new Tuple<int,int>(X,Y);
        }

        public override string ToString()
        {
            return string.Format("(x={0} y={1})", X, Y);
        }

        public override bool Equals(object obj)
        {
            return (((Coords) obj).X == this.X && ((Coords) obj).Y == this.Y);
        }

        public override int GetHashCode()
        {
            //Aggregated as strings to avoid equivalense of transposed points
            return ((X.GetHashCode().ToString() + Y.GetHashCode().ToString()).GetHashCode());
        }
    }
}
