﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;


namespace GazePswd
{
    class SaliencyAnalyzer
    {

        Bitmap img;

        private object _locker = new object();

        private String path;

        private double[,] map;
        
        //number of quadrants in the image
        int nQuadrants;

        //average quadrant saliency
        double avgSal;

        private Double allSum;

        //instance for singleton pattern
        public static SaliencyAnalyzer Instance;

        static SaliencyAnalyzer()
        {
            Instance = new SaliencyAnalyzer();
        }

        private SaliencyAnalyzer(){}
          
        public void CalculateSalMap(string path)
        {
            this.path = path;
            loadMap();
            lock (_locker)
            {
               

                allSum = 0.0;

                nQuadrants = Settings.QuadrantAmountX * Settings.QuadrantAmountY;
                avgSal = 1 / (double)nQuadrants;
                map = new double[Settings.QuadrantAmountX, Settings.QuadrantAmountY];
                for (int i = 0; i < Settings.QuadrantAmountX; i++)
                    for (int j = 0; j < Settings.QuadrantAmountY; j++)
                    {
                        //for each quadrant
                        int quadX = img.Width / Settings.QuadrantAmountX;
                        int quadY = img.Height / Settings.QuadrantAmountY;
                        for (int a = 0; a < quadX; a++)
                            for (int b = 0; b < quadY; b++)
                            {

                                Color ic = img.GetPixel(((i * quadX) + a), ((j * quadY) + b));
                                double gray = 0.2989 * ic.R + 0.5870 * ic.G + 0.1140 * ic.B + 2;

                                map[i, j] += gray == 0.0 ? 1 : gray;

                            }

                        //normalize pizels in quadrants


                        allSum += map[i, j];
                    }

                //normalizing all quadrants
                for (int i = 0; i < Settings.QuadrantAmountX; i++)
                    for (int j = 0; j < Settings.QuadrantAmountY; j++)
                    {
                        map[i, j] /= allSum;

                    }
            }
        }

        /// <summary>
        /// Returns the saliency value of the supplied password
        /// </summary>
        /// <param name="pass"></param>
        /// <returns></returns>
        public double calculateSalValue(IPassword pass)
        {

            double value = 1;

            //if the password contains no points, return.
            if (pass.Points.Count() == 0)
                return value;

            
            foreach (IPoint p in pass.Points)
            {
                value *= map[p.Coords.X, p.Coords.Y];
            }
            return value;
        }       
       
        /// <summary>
        /// returns the saliency value of a password, to the averge saliency of password of the same length
        /// </summary>
        /// <param name="pswd">A password</param>
        /// <returns>The saliency value of pswd, as a percentage of the average saliency for a password of the same length</returns>
        public Double PwdSaliencyToAvg(IPassword pswd)
        {
            double val = calculateSalValue(pswd);
            //average saliency of a pwd in n variables
            //note: the average saliency of a quadrant in the image is 1/nQuadrants
            return val/Math.Pow(avgSal,pswd.Points.Count());
        }

        public bool VerifyPassword(IPassword pwd){
            // may need tweaking.
            // the idea is that a pwd is okay, iff its saliency value is less
            // than the average for a pwd of its length.
            return PwdSaliencyToAvg(pwd) < Settings.LoginSaliencyBound;
        }

        /// <summary>
        /// loads the preprocessed saliency image
        /// </summary>
        /// <returns></returns>
        private void loadMap()
        {

            try
            {
                String[] s1 = path.Split('.');
                String[] s = s1[0].Split('/');
                String salPath = "saliencyImgs/" + s[1] + "_sm-Final.bmp";
                lock(_locker){
                    img = new Bitmap(Image.FromFile(salPath, true));
                }
                
  
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.Out.WriteLine("ERROR: Saliency image could not be found.");
                
            }
            
        }
    }
}
