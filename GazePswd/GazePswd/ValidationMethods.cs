﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    using Validation = Tuple<Access, String>;
    using WeightedValidation = Tuple<Tuple<Access, String>, int>;

    class ValidationMethods
    {
        
        //Can not be instantiated
        private ValidationMethods() { }

        /// <summary>
        /// Passwords with restrictions has less saliency test
        /// </summary>
        public static Validation validateTest1(OriginalPswd orig, AttemptPswd attempt)
        {
            Validation frechetDist = frechet(orig, attempt);
            Validation relaTimeDist = relativeTimeBucket(orig, attempt);
            Validation distanceDist = distanceComparer(orig, attempt);
            Validation pointProgressDist = pointProgression(orig, attempt);

            IEnumerable<Validation> denySet =  (new[] {frechetDist, relaTimeDist, distanceDist, pointProgressDist}).Where(val => Access.Denied == val.Item1);
            IEnumerable<Validation> acceptSet = (new[] { frechetDist, relaTimeDist, distanceDist, pointProgressDist });
            IEnumerable<Validation> hardAcceptSet = (new[] { frechetDist, relaTimeDist }).Where(val => Access.Granted == val.Item1);

            if (denySet.Any())
            {
                return new Validation(
                    Access.Denied, 
                    denySet.Aggregate("", (s, v2) => s + v2.Item2 + "\n"));
            }
            if (1 < acceptSet.Where(val => Access.Granted == val.Item1).Count() && 
                (0 < hardAcceptSet.Count()))
            {
                return new Validation(
                    Access.Granted, 
                    acceptSet.Where(acc => Access.Granted == acc.Item1).Aggregate("", (s, val) => s + val.Item2 + "\n"));
            }
            return new Validation(
                Access.Denied, 
                acceptSet.Where(acc => Access.Granted != acc.Item1).
                    Aggregate("", (s, acc) => 2 + acc.Item2 + "\n"));
        }

        /// <summary>
        /// Is it possible to LOGIN with saliency bounds?
        /// </summary>
        private Validation validateTest2(OriginalPswd orig, AttemptPswd attempt)
        {
            return checkRank(new WeightedValidation[]{
                new WeightedValidation(frechet(orig, attempt), Settings.DiscreteFrechetWeight),
                new WeightedValidation(totalTimeDifference(orig, attempt), Settings.TotalTimeDiffWeight),
                new WeightedValidation(pointProgression(orig, attempt), Settings.PointProgressionWeight),
                new WeightedValidation(distanceComparer(orig, attempt), Settings.DistanceComparerWeight),
                new WeightedValidation(relativeTimeBucket(orig, attempt), Settings.AbsoluteTimeBucketWeight)
            });
        }


        /// <summary>
        /// Given the path of the password, is it possible to hack the time analyzer?
        /// </summary>
        private void startTest5()
        {
        }

        /// <summary>
        /// Calculates the validation for frechet distance
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="attempt"></param>
        /// <returns></returns>
        private static Validation frechet(OriginalPswd orig, AttemptPswd attempt)
        {
            DiscreteFrechetAnalyser dfa = new DiscreteFrechetAnalyser();
            double dist = dfa.Match(orig, attempt);
            return thresholdToValidation(
                  Settings.frechetAccept
                , Settings.frechetDeny
                , dist
                , "Discrete Frechet distance: " + dist);
        }

        /// <summary>
        /// Checks the distance compared to the original password
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="attempt"></param>
        /// <returns></returns>
        private static Validation distanceComparer(OriginalPswd orig, AttemptPswd attempt)
        {
            DistanceAnalyzer da = new DistanceAnalyzer();
            double dist = da.Match(orig, attempt);
            return thresholdToValidation(
                  Settings.distanceComparerAccept
                , Settings.distanceComparerDeny
                , dist
                , "Distance diffference compared to original pswd: " + dist);
        }

        /// <summary>
        /// calculates the distance between the two passwords in point progression style
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="attempt"></param>
        /// <returns></returns>
        private static Validation pointProgression(OriginalPswd orig, AttemptPswd attempt)
        {
            PointProgressionAnalyzer pi = new PointProgressionAnalyzer();
            double dist = pi.Match(orig, attempt);
            return thresholdToValidation(
                  Settings.PointProgressionAccept
                , Settings.PointProgressionDeny
                , dist
                , "Point Progression distance: " + dist);
        }

        /// <summary>
        /// Calculate the validation of the general timing of the password attempt,
        /// note: this comes in a relative version as well.
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="attempt"></param>
        /// <returns></returns>
        private static Validation absoluteTimeBucket(OriginalPswd orig, AttemptPswd attempt)
        {
            TimeBucketAnalyzer ti = new TimeBucketAnalyzer(false);
            double result = ti.Match(orig, attempt);
            return thresholdToValidation(
                Settings.AbsoluteTimeBucketAccept,
                Settings.AbsoluteTimeBucketDeny,
                result,
                "AbsoluteTimeBucket diff: " + result
                );
        }

        /// <summary>
        /// This calculates the validation of the relative timing aspect of the attempt. 
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="attempt"></param>
        /// <returns></returns>
        public static Validation relativeTimeBucket(OriginalPswd orig, AttemptPswd attempt)
        {
            TimeBucketAnalyzer ti = new TimeBucketAnalyzer(true);
            double result = ti.Match(orig, attempt);
            return thresholdToValidation(
                Settings.relativeTimeBucketAccept,
                Settings.relativeTimeBucketDeny,
                result,
                "RelativeTimeBucket diff: " + result
                );
        }

        /// <summary>
        /// This calculates the validation based on the total startTime diference of the attempt compared to 
        /// the original pswd.
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="attempt"></param>
        /// <returns></returns>
        private static Validation totalTimeDifference(OriginalPswd orig, AttemptPswd attempt)
        {
            TotalTimeAnalyzer ti = new TotalTimeAnalyzer();
            double result = ti.Match(orig, attempt);
            return thresholdToValidation(
                Settings.TotalTimeDiffAccept,
                Settings.TotalTimeDiffDeny,
                result,
                "Total overall time Diff: " + result
                );
        }

        /// <summary>
        /// This checks the results of the validations and returns a validation enum
        /// based on the supplied limits
        /// </summary>
        /// <param name="lower">the closest difference bound that grants total acceptance</param>
        /// <param name="uppper">upper neutral acceptance, any higher than this and access is not granted</param>
        /// <param name="value">the actual result</param>
        /// <param name="message">the message to be delivered with the result</param>
        /// <returns></returns>
        private static Validation thresholdToValidation(double lower, double uppper, double value, String message)
        {
            Access result;
            if (value <= lower)
            {
                result = Access.Granted;
            }
            else if (value <= uppper)
            {
                result = Access.Neutral;
            }
            else
            {
                result = Access.Denied;
            }
            return new Validation(result, message);
        }

        /// <summary>
        /// Creates a unified validation based on the ranks of the different analyzers
        /// </summary>
        /// <returns>One validation to rule them all and forever in darkness...erhm.. never mind.</returns>
        private static Validation checkRank(WeightedValidation[] valArr)
        {
            StringBuilder sb = new StringBuilder();
            Access ac = Access.Granted;

            //Tests that ended up in neutral territory neutral
            List<WeightedValidation> rankOne = new List<WeightedValidation>();
            List<WeightedValidation> rankTwo = new List<WeightedValidation>();
            List<WeightedValidation> rankThree = new List<WeightedValidation>();

            //Failed validations.
            List<WeightedValidation> failures = new List<WeightedValidation>();

            for (int i = 0; i < valArr.Length; i++)
            {
                WeightedValidation wv = valArr[i];
                //If access was granted, skip to next.
                if ((Access)wv.Item1.Item1 == Access.Granted)
                {
                    Console.WriteLine(wv.Item2);
                    //We could add them to an accepted list here and write out the results.
                    continue;
                }

                //If the validation failed add it to failed list
                if ((Access)wv.Item1.Item1 == Access.Denied)
                {
                    failures.Add(wv);
                }
                //else add to ranked lists
                else
                {
                    switch (wv.Item2)
                    {
                        case 1:
                            rankOne.Add(wv);
                            continue;
                        case 2:
                            rankTwo.Add(wv);
                            continue;
                        case 3:
                            rankThree.Add(wv);
                            continue;
                    }
                }
            }

            //After all lists have been filled we evaluate the result

            //Add failures
            if (failures.Count > 0)
            {
                ac = Access.Denied;
                sb.AppendLine("Following tests did not pass:");
                foreach (WeightedValidation w in failures)
                {
                    //append result of failed validations
                    sb.AppendLine(w.Item1.Item2);
                }
            }
            //check if ranked lists amount to denial of access.
            if (rankOne.Count > 1)
            {
                ac = Access.Denied;
                sb.AppendLine("Following rank 1 neutrals exeeded limit:");
                foreach (WeightedValidation w in rankOne)
                {
                    //append result of neutral validations
                    sb.AppendLine(w.Item1.Item2);
                }
            }

            if (rankTwo.Count > 2)
            {
                ac = Access.Denied;
                sb.AppendLine("Following rank 2 neutrals exeeded limit:");
                foreach (WeightedValidation w in rankTwo)
                {
                    //append result of neutral validations
                    sb.AppendLine(w.Item1.Item2);
                }
            }

            //check if ranked lists amount to denial of access.
            if (rankThree.Count > 3)
            {
                ac = Access.Denied;
                sb.AppendLine("Following rank 3 neutrals exeeded limit:");
                foreach (WeightedValidation w in rankThree)
                {
                    //append result of neutral validations
                    sb.AppendLine(w.Item1.Item2);
                }
            }

            return new Validation(ac, sb.ToString());
        }


    }
}
