﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    interface IPipe<T>
    {
        void Write(T element);

        void Subscribe(Action<T> callback);

        void Flush();
    }
}
