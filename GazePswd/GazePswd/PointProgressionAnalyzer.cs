﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GazePswd
{
    using Bucket = Dictionary<Tuple<int, int>, int>;
    /// <summary>
    /// The sum of squared distances between the progression of points (pswd1i,pswd2i)
    /// </summary>
    class PointProgressionAnalyzer : IAnalyzer
    {
        /// <summary>
        /// Calculates the sq dist between the progressions of points   
        /// </summary>
        /// <param name="attempt"></param>
        /// <param name="orig"></param>
        /// <returns></returns>
        public double Match(OriginalPswd orig, AttemptPswd attempt)
        {
            //equalize password length
            padd(attempt, orig);
            int attemtBucketSize = attempt.Points.Count() / Settings.NumberOfPointProgressionBuckets;
            int origBucketSize = orig.Points.Count() / Settings.NumberOfPointProgressionBuckets;
            if (Settings.UseDwell)
            {
                attemtBucketSize = attempt.Points.Count();
                origBucketSize = orig.Points.Count();
            }
            Coords[] attemptBucketedCoords = divideBy(attempt, attemtBucketSize);
            Coords[] origBucketedCoords = divideBy(orig, origBucketSize);
            //sum of squares of euclidian distance
            return attemptBucketedCoords.Zip<Coords, Coords, double>(origBucketedCoords, (p1, p2) => euclidianDistance(p1, p2)).
                Select<double, double>(x => x*x).
                Aggregate((x, y) => x+y);
        }

        private void padd (IPassword pwd1, IPassword pwd2)
        {
            int delta = pwd2.Points.Count() - pwd1.Points.Count();
            IPoint padding = pwd1.Points.Last();
            for (int i = 0; i < delta; i++)
            {
                pwd1.Add(padding);
            }
        }

        public String Name
        {
            get { return "Point progression"; }
        }


        private double euclidianDistance(Coords a, Coords b)
        {
            return a.DistanceTo(b);
        }

        //returns the most common points, if points are divided into buckets of size n
        private Coords[] divideBy(IPassword pwd, int bucketSize)
        {
            
            LinkedList<Coords> list = new LinkedList<Coords>();

            Bucket pointCount = new Bucket();
            int i = 0;
            foreach (IPoint p in pwd.Points)
            {
                Tuple<int, int> t = new Tuple<int, int>(p.Coords.X, p.Coords.Y);
                if (pointCount.ContainsKey(t))
                {
                    pointCount[t]++;

                }
                else
                {
                    pointCount.Add(t, 1);
                }

                //if we got enough for one bucket
                if ((i + 1) % bucketSize == 0)
                {
                    //this selects the tuple with the max visited value
                    KeyValuePair<Tuple<int,int>,int> kvPoint = pointCount.Aggregate((e1, e2) => Math.Max(e1.Value, e2.Value) == e1.Value ? e1 : e2);
                   
                    Coords cds = new Coords();
                    cds.X = kvPoint.Key.Item1;
                    cds.Y = kvPoint.Key.Item2;
                    
                    list.AddLast(cds);
                }
                ++i;
            }
            return list.ToArray();
        }
    }
}
