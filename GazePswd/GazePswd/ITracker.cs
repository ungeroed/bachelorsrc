﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    interface ITracker
    {
        void TrackingEnabled(bool enabled);
    }
}
