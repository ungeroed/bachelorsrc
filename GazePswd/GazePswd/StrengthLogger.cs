﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class StrengthLogger : IPlumable<String, String>
    {
        private PasswordId currentUser;

        private Action<string> del;

        private bool set = false;

        private IPersist persister;

        /// <summary>
        /// public constructor for your pleasure...
        /// </summary>
        public StrengthLogger()
        {
            del = new Action<string>(callback);
            persister = new DiskPersist();
        }

        /// <summary>
        /// callback method run by the delegate
        /// </summary>
        /// <param name="s"></param>
        private void callback(String s)
        {
            List<String> ls = new List<string>();
            ls.Add(s);
            if (set)
            {
                persister.WriteStrengthData(currentUser, s);
            }
        }

        /// <summary>
        /// Assigns the current username, and creates the logfile
        /// </summary>
        /// <param name="user"></param>
        public void SetUser(PasswordId user)
        {
            currentUser = user;   
            set = true;
        }

        public void Reader(IPipe<string> connection)
        {
            connection.Subscribe(del);
        }

        public void Writer(IPipe<string> connection)
        {
            throw new NotImplementedException();
        }
    }
}
