------------------------<README.txt>--------------------------
GazeTracker helpfile.
Valid commands:
"start": enables tracking
"stop": disables tracking
"quit": quits the program
"exit": quits the program
"setup": enters setup mode
"show grid": shows the underlying grid
"clear": clears the image,
"show password": shows the password path
"restart": restarts the program.
"show quads": shows the quadrants used in your password
"show box": shows the bounding box of the password
"save attempts": saves all password attempts
"disable attempts": disables saving of attempts
"show attempts": draws all password attempts                   
"clear attempts": Deletes all attempts of the current user.
"process attempts": processes all attempts
"next": cycles to the next password image
"toggle gaze": toggles the live drawing of gaze points
"use dwell" switches to dwell time selection
"disable dwell" disables dwell time selection.
"show saliency" show the saliency image for the current image. 
"help": displays this text. 
-------------------------------------------------------------
