﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GazePswd
{
    class AsyncPipe<T> : IPipe<T>
    {
        Action<T> handler;

        public AsyncPipe()
        {
            Thread t1 = new Thread(() => processQueue(this));
            t1.IsBackground = true;
            t1.Start();
            
        }

        public void Write(T element)
        {
            
            queue.Add(element);
        }

        void IPipe<T>.Subscribe(Action<T> callback)
        {
            handler += callback;
        }

        void IPipe<T>.Flush()
        {
            handler = null;
        }

        //This queue is threadsafe.
        private BlockingCollection<T> queue = new BlockingCollection<T>(new ConcurrentQueue<T>());
        
        private void processQueue(AsyncPipe<T> p){
            while(true)
            {
                var packet = queue.Take(); //this blocks if there are no items in the queue.
                if (p.handler != null)
                {
                    
                    p.handler.Invoke(packet);//Send your packet here.
                }
            }
        }

    }
}
