﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace GazePswd
{
    class MouseTracker : IPlumable<IPoint,IPoint>, ITracker
    {

        //Turns tracking on and off. SPACEBAR TOGGLES ON AND OFF
        private bool track = true;

        //the last point stored
        private DateTime lastPoint;

        //To hold some plumbing
        IPipe<IPoint> writer;

        public static MouseTracker Instance;

        static MouseTracker()
        {
            Instance = new MouseTracker();
        }

        /// <summary>
        /// Private constructor per the singleton pattern
        /// </summary>
        private MouseTracker()
        {
            lastPoint = DateTime.Now;
            track = false;
        }

        /// <summary>
        /// Mouse Event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="preBounding"></param>
        public void CaptureMouse(object sender, MouseEventArgs e)
        {
            int currentImageX = ((MainView)sender).CurrentPswdImage.Width;
            int currentImageY = ((MainView)sender).CurrentPswdImage.Height;
            //If we're not inside the password image, we return
            if (e.X > currentImageX || e.Y > currentImageY)
            {
                return;
            }
            //If tracking is turned on, do some tracking :-) 
            if (track)
            {
                storePoint(e.X, e.Y, currentImageX, currentImageY);
            }
            
        }
 

        ///// <summary>
        ///// Method that stores a point if 1/20 of a second has passed.
        ///// </summary>
        ///// <param name="x">x coord</param>
        ///// <param name="y">y coord</param>
        public void storePoint(int x, int y, int imageWidth, int imageHeight)
        {
            if (writer == null) return;
            //add point to pointqueue if the 1/20 of a second has passed. 1000 miliseconds = 1 second, 1000/20 = 50ms
            DateTime jetzt = DateTime.Now;
            if ((jetzt - lastPoint).TotalMilliseconds > 50)
            {
                Point newPoint = new Point();
                newPoint.PixelCoords = new Coords(x, y);
                newPoint.PixelBound = new Coords(imageWidth, imageHeight);
                newPoint.Time = jetzt;
                if(writer != null)
                    writer.Write(newPoint);
            }

        }

        public void Reader(IPipe<IPoint> connection)
        {
            new NotImplementedException();
        }

        public void Writer(IPipe<IPoint> connection)
        {
            writer = connection;
        }

        public void TrackingEnabled(bool enabled)
        {
            track = enabled;
        }
    }
}
