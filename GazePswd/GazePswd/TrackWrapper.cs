﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tobii.EyeX.Client;
using Tobii.EyeX.Framework;

namespace GazePswd
{
    class TrackWrapper : IPlumable<IPoint, IPoint>, ITracker
    {
        
             
        private const string InteractorId = "minimal";

        private InteractionContext _context;
        private InteractionSnapshot _globalInteractorSnapshot;

        private InteractionSystem system = InteractionSystem.Initialize(LogTarget.Trace);
        //writer object
        private IPipe<IPoint> writer;

        public TrackWrapper()
        {
            
            // create a context, register event handlers, and enable the connection to the engine.
            _context = new InteractionContext(false);
            InitializeGlobalInteractorSnapshot();
            _context.ConnectionStateChanged += OnConnectionStateChanged;
            _context.RegisterEventHandler(HandleEvent);
            _context.EnableConnection();
           

        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }


       

        private void OnConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            //Console.WriteLine("The connection state is now {0}.", preBounding.State);

            if (e.State == ConnectionState.Connected)
            {
                // commit the snapshot with the global interactor as soon as the connection to the engine is established.
                // (it cannot be done earlier because committing means "send to the engine".)
                _globalInteractorSnapshot.Commit(OnSnapshotCommitted);
                
            }
            
        }

        private void InitializeGlobalInteractorSnapshot()
        {
            _globalInteractorSnapshot = _context.CreateSnapshot();
            _globalInteractorSnapshot.CreateBounds(InteractionBoundsType.None);
            _globalInteractorSnapshot.AddWindowId(Literals.GlobalInteractorWindowId);

            var interactor = _globalInteractorSnapshot.CreateInteractor(InteractorId, Literals.RootId, Literals.GlobalInteractorWindowId);
            interactor.CreateBounds(InteractionBoundsType.None);

            var behavior = interactor.CreateBehavior(InteractionBehaviorType.GazePointData);
            var behaviorParams = new GazePointDataParams() { GazePointDataMode = GazePointDataMode.LightlyFiltered };
            behavior.SetGazePointDataOptions(ref behaviorParams);

            _globalInteractorSnapshot.Commit(OnSnapshotCommitted);
        }

        private void OnSnapshotCommitted(InteractionSnapshotResult result)
        {
            Debug.Assert(result.ResultCode != SnapshotResultCode.InvalidSnapshot, result.ErrorMessage);
        }

        private void HandleEvent(InteractionEvent @event)
        {
            foreach (var behavior in @event.Behaviors)
            {
                if (behavior.BehaviorType == InteractionBehaviorType.GazePointData)
                {
                    
                    OnGazePointData(behavior);
                }
            }
        }

        /// <summary>
        /// Enables or disables tracking
        /// </summary>
        /// <param name="enabled"></param>
        public void TrackingEnabled(bool enabled)
        {
           TrackingDataValidator.enableTracking = enabled;
        }

        private void OnGazePointData(InteractionBehavior behavior)
        {
            GazePointDataEventParams eventParams;
            if (behavior.TryGetGazePointDataEventParams(out eventParams))
            {
                IPoint pt = new Point();
                Coords coords = new Coords();
                coords.X = (int)eventParams.X;
                coords.Y = (int)eventParams.Y;
                pt.ScreenCoords = coords;
                pt.Time = DateTime.Now;
                if(writer != null)
                    writer.Write(pt);
            }
            else
            {
                Console.WriteLine("Failed to interpret gaze data event packet.");
            }
        }

     
 

        [DllImport("user32", SetLastError = true)]
        private static extern bool SetProcessDPIAware();

        public void Reader(IPipe<IPoint> connection)
        {
            throw new NotImplementedException();
        }

        public void Writer(IPipe<IPoint> connection)
        {
            writer = connection;
        }
    }
        
    
}
