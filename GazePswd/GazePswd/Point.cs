﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace GazePswd
{
    public struct Point : IPoint
    {
       public Coords Coords {get; set;}
       public DateTime Time{ get; set;}
       public Coords PixelCoords { get; set; }
       public Coords CoordAmount {get; set;}
       public Coords PixelBound {get; set;}
       public Coords ScreenCoords { get; set; }

        /// <summary>
        /// Simple ToString override, but is used for basic serialization.
        /// Format is semantic, and depended upon by Point.FromString()
        /// </summary>
        /// <returns>The point represented as a string</returns>
        public override string ToString()
        {
            //the order matters, as it it used in FromString
            return Coords.ToString() + ", " + CoordAmount.ToString() + ", " + PixelCoords.ToString() + ", " + PixelBound.ToString() + ", " + Time.Ticks;
        }

        /// <summary>
        /// Takes a string, formateed as by Point.ToString() and returns a point from it
        /// Basic desrialization
        /// </summary>
        /// <param name="pointString">String as from Point.ToString()</param>
        /// <returns>Point corresponding to the one represented by pointString</returns>
        static public Point FromString(String pointString)
        {
            Point p = new Point();
            p.Coords = new Coords();
            p.CoordAmount = new Coords();
            p.Time = new DateTime();
            p.PixelCoords = new Coords();
            p.PixelBound = new Coords();
            
            Regex coord = new Regex("[0-9]+");
            String[] fields = pointString.Split(new Char[1]{','});
            
            //coords
            Match ints = coord.Match(fields[0]);
            int coordX = Convert.ToInt32(ints.Value);
            int coordY = Convert.ToInt32(ints.NextMatch().Value);
            p.Coords = new Coords(coordX, coordY);

            //CoordAmount            
            ints = coord.Match(fields[1]);
            int coordAmountX = Convert.ToInt32(ints.Value);
            int coordAmountY = Convert.ToInt32(ints.NextMatch().Value);
            p.CoordAmount = new Coords(coordAmountX,coordAmountY);

            //pixelCoords
            ints = coord.Match(fields[2]);
            int pixelX = Convert.ToInt32(ints.Value);
            int pixelY = Convert.ToInt32(ints.NextMatch().Value);
            p.PixelCoords = new Coords(pixelX, pixelY);

            //pixelbound
            ints = coord.Match(fields[3]);
            int pixelBoundX = Convert.ToInt32(ints.Value);
            int pixelBoundY = Convert.ToInt32(ints.NextMatch().Value);
            p.PixelBound = new Coords(pixelBoundX, pixelBoundY);

            //startTime
            DateTime timestamp = new DateTime(long.Parse(fields[4]));
            p.Time = timestamp;
            return p;
        }
    }
}
