﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    interface IAnalyzer
    {
        String Name { get; }
        Double Match(OriginalPswd orig, AttemptPswd attempt);

    }
}
