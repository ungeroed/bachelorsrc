﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class SetupPswdAnalyzer : IPlumable<IPassword, string>
    {

        //Delegate to handle incomming IPoints
        private Action<IPassword> del;

        //Pipe to write to
        private IPipe<string> writer;

        private bool Approved;

        private IPassword Pswd;

        private string CurrentFindings;

        public SetupPswdAnalyzer()
        {
            Approved = false;
            del = new Action<IPassword>(VerifyPasswordDetails);
            CurrentFindings = "Findings Not calculated yet";
        }

        public bool GetApproved() { return Approved; }
        public void ResetApproved() { Approved = false; }
        public IPassword GetPswd() { return Pswd; }
        public string GetFindings() { return CurrentFindings; }


        /// <summary>
        /// Instantiates and comments on the supplied password
        /// </summary>
        /// <param name="pswd"></param>
        private void VerifyPasswordDetails(IPassword pswd)
        {
            this.Pswd = pswd;


            //if salAnalyzer is not yet instantiated TODO: instantiate other place
           
            SaliencyAnalyzer salAnalyzer = SaliencyAnalyzer.Instance;

            Double preSaliency = salAnalyzer.PwdSaliencyToAvg(pswd);
            

            //Check the bounding box
            BoundingBoxer boxer = new BoundingBoxer();
            double preBounding = boxer.GetBoundingPercent(pswd);
            
            //check total euclidian distance
            DistanceAnalyzer distanceAnalyzer = new DistanceAnalyzer();
            double preDistance = distanceAnalyzer.GetDistancePercent(pswd);


            CurrentFindings = "SaliencyPercent: " + preSaliency + "Should be under: "+ Settings.PreSaliencyThreshold + "\n" +
                    "BoundingBox: " + preBounding + "% Should be at least: " + Settings.PreBoundingBoxThreshold + "% \n" +
                    "Distance: " + preDistance + "% Should be at least: "+ Settings.PreDistanceThreshold +"% \n" +
                    "Number of Points recorded: " + pswd.Points.Count() + " should be at least: " + Settings.PrePointNumberThreshold;

            //THis is where we set the threshold
            if (pswd.Points.Count() >= ((Settings.UseDwell) ? Settings.PrepointNUmberDwellThreshold : Settings.PrePointNumberThreshold) && 
                preSaliency <= Settings.PreSaliencyThreshold && 
                preBounding >= Settings.PreBoundingBoxThreshold && 
                preDistance >= Settings.PreDistanceThreshold)
            {
                Approved = true;
            }


            if (writer != null)
            {
                writer.Write(CurrentFindings);
            }
        }

        public void Reader(IPipe<IPassword> connection)
        {
            connection.Subscribe(del);
        }

        public void Writer(IPipe<string> connection)
        {
            writer = connection;
        }

       
    }
}
