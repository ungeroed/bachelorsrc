﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GazePswd
{
    class BoundingBoxer
    {
        public Rect GetBoundingBox(IPassword pass)
        {
            int minX = int.MaxValue;
            int maxX = int.MinValue;

            int minY = int.MaxValue;
            int maxY = int.MinValue;

            //Calculates min and max x & y values.
            foreach (IPoint p in pass.Points)
            {
                if (p.Coords.X > maxX)
                    maxX = p.Coords.X;
                
                if (p.Coords.X < minX)
                    minX = p.Coords.X;

                if (p.Coords.Y > maxY)
                    maxY = p.Coords.Y;
               
                if (p.Coords.Y < minY)
                    minY = p.Coords.Y;
            }

            return new Rect(minX, minY, maxX - minX, maxY - minY);

        }

        public double GetBoundingPercent(IPassword pass)
        {
            if (pass.Points.Count() == 0)
                return 1.00;
            Rect rect = GetBoundingBox(pass);
            int imageArea = pass.Points.First().CoordAmount.X * pass.Points.First().CoordAmount.Y; //lossless -- int^int -> int
            int boxArea = rect.Height * rect.Width;
            double percentage = (boxArea / (double)imageArea);
            return percentage;
        }
    }
}
