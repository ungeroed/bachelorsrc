﻿using System;
using System.Collections.Generic;
using System.Collections;


namespace GazePswd
{
   
    public class SimplePassword : IPassword, OriginalPswd, AttemptPswd
    {
        LinkedList<IPoint> points = new LinkedList<IPoint>();
     
        public IEnumerable<IPoint> Points
        {
            get { return points; }
        }

        public void Add(IPoint point)
        {
            points.AddLast(point);
        }

        public String ImgPath { get; set; }

        public IPassword Clone()
        {
            
            return new SimplePassword() {points = cloneLinkedList(points)};
        }

        /// <summary>
        /// returns a shallow copy of the list
        /// </summary>
        /// <param name="origLst"></param>
        /// <returns></returns>
        private static LinkedList<IPoint> cloneLinkedList(LinkedList<IPoint> origLst)
        {
            LinkedList<IPoint> newLst = new LinkedList<IPoint>();
            foreach (IPoint p in origLst)
            {
                newLst.AddLast(p);
            }
            return newLst;
        }

    }
}
