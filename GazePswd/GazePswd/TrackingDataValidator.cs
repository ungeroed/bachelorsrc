﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazePswd
{
    class TrackingDataValidator : IPlumable<IPoint, IPoint>
    {
        //writer object
        private IPipe<IPoint> writer;

        //delegate that handles incomming points
        private Action<IPoint> del;

        //enables or disables tracking
        public static bool enableTracking = false;

        //The main view
        MainView view;

        /// <summary>
        /// Public constructor.. nuff said. 
        /// </summary>
        public TrackingDataValidator(MainView v)
        {
            view = v;
            del = new Action<IPoint>(callback);
        }

        /// <summary>
        /// Callback that handles all incommin points
        /// </summary>
        /// <param name="point"></param>
        private void callback(IPoint point)
        {
            
            //if were not on view thread, which we're never at this point
            if (view.InvokeRequired)
            {
                view.Invoke(new Func<IPoint, int>(invokeOnViewThread), new Object[] { point });
            }
            else
            {
                invokeOnViewThread(point);
            }
        }

        /// <summary>
        /// Returns true if the coordinates of the point lies withing the image border
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private bool withinBounds(System.Drawing.Point p)
        {
            //Test for below zero coords = outside frame
            if (p.X < 0 || p.Y < 0)
                return false;
            int imgX = view.CurrentPswdImage.Width;
            int imgY = view.CurrentPswdImage.Height;
            if (p.X > imgX || p.Y > imgY)
                return false;

            return true;
        }

        private int invokeOnViewThread(IPoint point)
        {
            //Drawing the gaze if enabled in view
            System.Drawing.Point screenCoords = new System.Drawing.Point(point.ScreenCoords.X, point.ScreenCoords.Y);

            System.Drawing.Point imagePoint = view.PointToClient(screenCoords);

            view.DrawGaze(imagePoint);

            //If they fit within bounds of image pass the point on to the pipe.
            if (withinBounds(imagePoint))
            {
                point.PixelCoords = new Coords(imagePoint.X, imagePoint.Y);
                point.PixelBound = new Coords(view.CurrentPswdImage.Width, view.CurrentPswdImage.Height);
                if (enableTracking)
                {
                    writer.Write(point);
                }
         
            }
            return 0;
        }

        public void Reader(IPipe<IPoint> connection)
        {
            connection.Subscribe(del);
        }

        public void Writer(IPipe<IPoint> connection)
        {
            writer = connection;
        }
    }
}
